#ifndef REGISTER_PRESSURE_RELEASER_H
#define REGISTER_PRESSURE_RELEASER_H
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachinePostDominators.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineLoopInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/LiveInterval.h"
#include "llvm/CodeGen/LiveStackAnalysis.h"
#include "llvm/CodeGen/RegisterClassInfo.h"

#include "llvm/ADT/PostOrderIterator.h"
#include "llvm/IR/CFG.h"

#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetRegisterInfo.h"

#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetOpcodes.h"

#include <utility>
#include "NextAccessAnalysis.h"

namespace llvm{
class LiveStacks;

  typedef struct RegInfo{
    unsigned virt;
    unsigned phys;
    unsigned dist;

    RegInfo(unsigned v): virt(v), phys(0), dist(0){}
    RegInfo(unsigned v, unsigned p, unsigned d): virt(v), phys(p), dist(d){}
    RegInfo(RegInfo* info): virt(info->virt), phys(info->phys), dist(info->dist){}
    void SetVirt(unsigned v){virt = v;}
    void SetPhys(unsigned p){phys = p;}
    void SetDist(unsigned d){dist = d;}
    void ClearPhys(){phys = 0;}
  }RegInfo;

  struct RegDistCompare{
    MachineFunction* mf;

    RegDistCompare(): mf(NULL){}
    RegDistCompare(MachineFunction* MF): mf(MF){}

    //no physreg in regdists
    // no spill for physreg
    bool operator() (const RegInfo* lhs, const RegInfo* rhs) const{
      unsigned dist1 = lhs->dist;
      unsigned dist2 = rhs->dist;
      if (dist1 == dist2){
          unsigned reg1 = lhs->virt;
          unsigned reg2 = rhs->virt;
          
          MachineRegisterInfo* mri = &mf->getRegInfo();
          const TargetRegisterInfo *tri = mf->getTarget().getRegisterInfo();

          const TargetRegisterClass* rc1 = mri->getRegClass(reg1);
          const TargetRegisterClass* rc2 = mri->getRegClass(reg2);
          const RegClassWeight& wt1 = tri->getRegClassWeight(rc1);
          const RegClassWeight& wt2 = tri->getRegClassWeight(rc2);

          //weight limit is how much freedom it has
          if (wt1.WeightLimit== wt2.WeightLimit){
            if (wt1.RegWeight == wt2.RegWeight)
              return reg1 > reg2;
            return wt1.RegWeight > wt2.RegWeight;
          }
          
          //weight limit is how much freedom it has
          return (wt1.WeightLimit > wt2.WeightLimit);
      }

      //one has to be less
      return dist1 > dist2;
    }
  };

#define RPR_DEBUG
#ifndef RPR_DEBUG
  //"converting" opr to unsigned reg
  struct SimpleRegCompare{
    SimpleRegCompare(MachineFunction* MF): mf(MF) {}
    SimpleRegCompare(): mf(NULL) {}

    bool operator() (unsigned reg1, unsigned reg2){
      if (reg1 == reg2)
        return false;
      const TargetRegisterInfo* tri = mf->getTarget().getRegisterInfo();
      MachineRegisterInfo* mri = &mf->getRegInfo();;

      const TargetRegisterClass* rc1 = mri->getRegClass(reg1);
      const TargetRegisterClass* rc2 = mri->getRegClass(reg2);
      const RegClassWeight& wt1 = tri->getRegClassWeight(rc1);
      const RegClassWeight& wt2 = tri->getRegClassWeight(rc2);

      if (wt1.WeightLimit == wt2.WeightLimit){
        if (wt1.RegWeight == wt2.RegWeight)
          return reg1 < reg2;
        return wt1.RegWeight < wt2.RegWeight;
      }

      //weight limit is how much freedom it has
      return (wt1.WeightLimit < wt2.WeightLimit);
    }
    MachineFunction* mf;
  };
#else
  //"converting" opr to unsigned reg
  struct SimpleRegCompare{
    SimpleRegCompare(MachineFunction* MF, 
          std::map<const TargetRegisterClass*, ArrayRef<MCPhysReg>>* regOrders): 
          mf(MF), orders(regOrders) {}
    SimpleRegCompare(): mf(NULL), orders(NULL){}

    bool operator() (unsigned reg1, unsigned reg2){
      if (reg1 == reg2)
        return false;
      const TargetRegisterInfo* tri = mf->getTarget().getRegisterInfo();
      MachineRegisterInfo* mri = &mf->getRegInfo();;
  
      const TargetRegisterClass* rc1 = mri->getRegClass(reg1);
      const TargetRegisterClass* rc2 = mri->getRegClass(reg2);
      const RegClassWeight& wt1 = tri->getRegClassWeight(rc1);
      const RegClassWeight& wt2 = tri->getRegClassWeight(rc2);
      unsigned size1 = orders->at(rc1).size();
      unsigned size2 = orders->at(rc2).size();
  
      if (size1 == size2){
        if (wt1.RegWeight == wt2.RegWeight)
          return reg1 < reg2;
        return wt1.RegWeight < wt2.RegWeight;
      }
  
      //weight limit is how much freedom it has
      return (size1 < size2);
    }
    MachineFunction* mf;
    const std::map<const TargetRegisterClass*, ArrayRef<MCPhysReg>>* orders; 
  };
#endif
  /*record spill locations for a given reg*/
  typedef std::map<unsigned, std::vector<MachineInstr*>> SpillInstrs;
  typedef std::map<MachineBasicBlock*, SpillInstrs> SpillMap;
  typedef std::map<unsigned, int> SlotMap;
  typedef std::set<unsigned> CurLivePhysRegs;
  typedef std::map<MachineBasicBlock*, CurLivePhysRegs> CurLivePhysMap;
  typedef std::set<unsigned, SimpleRegCompare> CurLiveRegs;
  typedef std::map<MachineBasicBlock*, CurLiveRegs> CurLiveMap;
  typedef std::set<RegInfo*, RegDistCompare> RegDists;
  typedef std::map<MachineBasicBlock*, RegDists> DistMap;
  //collection of all the reg infos, can be used to free
  typedef std::vector<RegInfo*> RINFOS;
  //this gives all the (virt ->) information
  typedef std::map<unsigned , RegInfo*> VIRT2INFO;
  typedef std::map<MachineBasicBlock*, VIRT2INFO> VIRT2INFOS;

  //at what location is a register unit free
  typedef std::map<unsigned, MachineInstr*> RU2LOC;
  typedef std::map<MachineBasicBlock*, RU2LOC> PressureChangeMap;


  class RegisterPressureReleaser: public MachineFunctionPass{
    private:
      //for this transformation
      //reg class info
      bool         FTR;//first time running
      unsigned actualNumRegs;
      unsigned  maxRCW;//max register class weight

      /*========================RESOURSES==================================*/
      // need to clean the below resoureces

      /*--------pressure related--------------------*/
      RegisterClassInfo RCI;
      std::map<const TargetRegisterClass*, ArrayRef<MCPhysReg>> regOrders;

      //Units whose complements are not occupied
      //  TODO: use it to recalculate distance regarding under utilizing units
      std::set<unsigned> underUtilRegUnits;//probably not needed

      //units to operand mapping for fake try assign, we can get reg class from opr
      std::map<MachineBasicBlock*, std::map<unsigned, unsigned>> ru2regs;

      //this gives virt->phys, virt->dist
      VIRT2INFOS virt2infos;

      /*--------------live and spill and such--------------------*/
      CurLiveMap liMap;//liveins of bbs
      CurLiveMap loMap;//liveouts of bbs
      CurLivePhysRegs livePhysRegs;//assume only live within BB's

      //TODO some struct spill doms
      SpillMap  spills;  //where the spill decisions are made
      SpillMap actualSpills;
      SlotMap   slots;   //where the vrs are spilled to
      std::set<unsigned> spilledRegs;//collection of all spilled regs
      SpillMap  restores;//where the dominating spills are
      DistMap   regDists;//dists

      /*----how far can you push a store?
       *----  as far as the reg unit it requires becomes free
       *----when does a reg unit become free
       *----  the value in the reg unit dies: no more uses or all defs after a use
       *----  either actually dies or spilled later
       *----  so last use that's live or some BB begin if multiply preds don't agree
       */
      PressureChangeMap pressureChangePoints;

      //register units that might have been used
      //  might Used RU's are available at earliest at recoloring location
      //  others are available at bb begin
      std::set<unsigned> mightUsedRU;
      std::map<MachineBasicBlock*, MachineInstr*> recolorLoc;

      std::map<MachineBasicBlock*, std::set<unsigned>> spillIn;
      std::map<MachineBasicBlock*, std::set<unsigned>> spillOut;
      std::map<MachineBasicBlock*, std::set<unsigned>> spillKill;

      std::map<unsigned, LiveInterval*> stackInts;

      /*liveout of back edge block must agree with livein of it's pred block
       * keys: bb edge tails
       * values: bb edge heads
       */
      std::map<MachineBasicBlock*, std::set<MachineBasicBlock*>> restrictedBlockEdges;
      std::vector<MachineBasicBlock*> blocksToErase;
      std::vector<MachineInstr*> fakeBranchesToErase;

      // need to clean the above resoureces
      /*===============END OF RESOURCES=============================*/

      //info
      MachineFunction* MF;
      MachineRegisterInfo* MRI;
      const TargetMachine* TM;
      const TargetRegisterInfo *TRI;
      const TargetInstrInfo *TII;

      //analysis
      MachineDominatorTree* DT;
      MachinePostDominatorTree* PDT;
      SlotIndexes* SLOTINDEXES;
      LiveIntervals* LIS;//for distances and indexes(indices)
      LiveStacks* LSS;//to pass down to greedy
      VirtRegMap* VRM;//for spills and such
      NextAccessAnalyzer* NAA;
      MachineLoopInfo *Loops;

      /*---------------DEBUGGING RELATED-------------------------------*/
      void PrintAllocatableRegs();
      void PrintVirtSet(CurLiveRegs& virts);
      void PrintLiveVirts(MachineBasicBlock* mbb);
      void PrintLivePhys();
      unsigned GetReg(CurLiveRegs& virts, unsigned index);
      bool RegCompare(unsigned reg1, unsigned reg2);
      void PrintRegDists(MachineBasicBlock* mbb);
      void PrintRegInfo(MachineBasicBlock* mbb);
      void PrintOrder(const TargetRegisterClass* rc);
      void PrintLiveIntervals();
      void PrintRestoreLocations();
      void PrintSpillLocations();



      /*---------------Spilling related--------------------------------*/

      //check if RC contains an alias of physReg (including physReg)
      bool ShareRegUnits(const TargetRegisterClass* RC1, const TargetRegisterClass* RC2);

      //spill a given reg after a given location
      void SpillRegAfter(unsigned reg, MachineInstr* spillLoc,
                         const TargetRegisterClass* trc, unsigned slot);

      //spill a given reg at a given location
      void SpillRegAt(unsigned spillReg, MachineInstr* spillLoc,
                      const TargetRegisterClass* trc, unsigned slot);

      //spill after all defs dominated by spillLoc
      void SpillAfterDef(unsigned regDef, MachineInstr* spillInstr);

      void RestoreRegAt(unsigned restoreReg, MachineInstr* restoreLoc,
                        const TargetRegisterClass* trc,unsigned slot);

      void PushSpillUp(unsigned reg, MachineBasicBlock* mbb);
      bool CanPushToPred(unsigned reg, unsigned loopDepth, MachineBasicBlock* pred);


      //spill a reg at a given location
      MachineInstr* SpillReg(unsigned spillReg, MachineInstr* mi, MachineBasicBlock* spillBlock);


      //spill one of the live virtual registers whose class shares reg(s) with
      //                            a given high pressure reg class
      MachineInstr* Spill(MachineInstr* mi, const TargetRegisterClass* RC, MachineBasicBlock* spillBlock);


      //restore a virtual reg at mi
      void Restore(unsigned reg, MachineInstr* mi);

      //insert actual load and stores for restores and spills
      void AddSpillInstructions();
      void AddRestoreInstructions();
      void AddLoadAndStores();

      //TODO: map of a queue of spill locations that have no dominance relationship.
      /*
       * Spill block is the block in which a value is spilled and the block has max path coverage.
       */
      MachineBasicBlock* FindSpillBlock(unsigned reg, MachineBasicBlock* MBB);

      bool AccessReg(unsigned reg, MachineInstr* MI);

      MachineInstr* GetInsertedInstr(MachineBasicBlock* mbb, MachineInstr* insertLoc, unsigned reg, bool isSpill);

      //-------------For merging--------------
      void DealWithBBEdges();

      void SpillAndRestoreAtBBEdge(MachineBasicBlock* pred, MachineBasicBlock* succ);
      void PrepBBBegin(MachineBasicBlock* curBB);

      MachineInstr* GetTerminator(MachineBasicBlock* predBB, MachineBasicBlock* succBB);

      void InitLiveStackAnalysis();
      //Add live stack intervals needed later
      void AddLiveStackIntervals();

      //------------Add necessary restores and spills--------
      //  make sure that for each (def, restore) pair where
      //        def < restore and restore is reachable from def
      //        there is a spill in between
      //  NOTE: can't guarentee no redundant spills on certain paths
      void InsertRestoreLocation(unsigned reg,  MachineInstr* location, MachineBasicBlock* mbb);
      void InsertSpillLocation(unsigned reg, MachineInstr* location, MachineBasicBlock* mbb);
      void MapPressurePoint(unsigned reg, MachineInstr* location, MachineBasicBlock* mbb);


      void InitRestoreCheck();
      void InitSpillCheck();
      //spill placement
      void PropSpillInUpWard();
      void PropSpillOutDownWard();
      void AddRemaningSpillsAndRestores();

      //live range updates for spilled values and stack locations
      void UpdateSpilledLiveRanges();

      /*---------------Register pressure related------------------------*/
      //make simple orders for regs in a reg class and mroe
      void GetMoreRCI();
      //assign physReg to its units and try to assigned virts that occupied the units
      //  if can't assign easily, return false and expect recolor
      bool TryAssignPhysReg(unsigned physReg, MachineBasicBlock* mbb);

      //Try to find free reg units to fit a virt reg
      bool TryAssignVirtOnce(unsigned reg, MachineBasicBlock* mbb);

      //Try to find proper reg units for reg, try reassign at most once
      //  result can be used to describe what values cause the pressure
      const TargetRegisterClass* TryAssign(unsigned reg, MachineBasicBlock* mbb);

      //assignment at the beginning of curBB
      const TargetRegisterClass* TryColor(MachineBasicBlock* curBB);

      //recolor according to orders
      const TargetRegisterClass* Recolor(CurLiveRegs& regs, MachineBasicBlock* mbb);

      //help function to clear out the vr 2 pr mapping in reg infos
      void ClearVR2PR(MachineBasicBlock* mbb);
      /*---------------END OF Register pressure related-----------------*/

      void ReleaseRegisterPressure();
      void init();

    public:
      static char ID; // Pass identification, replacement for typeid
      RegisterPressureReleaser();
      virtual ~RegisterPressureReleaser();
      void getAnalysisUsage(AnalysisUsage &AU) const override;
      void releaseMemory() override;
      /// runOnMachineFunction - pass entry point
      bool runOnMachineFunction(MachineFunction&) override;
  };
}
#endif //REGISTER_PRESSURE_RELEASER_H

