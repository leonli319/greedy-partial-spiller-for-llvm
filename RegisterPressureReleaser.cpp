#include "RegisterPressureReleaser.h"
#include "NextAccessAnalysis.h"

#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/PostOrderIterator.h"
#include "llvm/ADT/Statistic.h"

#include "llvm/IR/CFG.h"
#include "llvm/IR/Dominators.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"

#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/LiveInterval.h"
#include "llvm/CodeGen/LiveIntervalAnalysis.h"
#include "llvm/CodeGen/LiveRangeEdit.h"
#include "llvm/CodeGen/LiveStackAnalysis.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachineLoopInfo.h"
#include "llvm/CodeGen/MachinePostDominators.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/RegAllocRegistry.h"
#include "llvm/CodeGen/RegisterClassInfo.h"
#include "llvm/CodeGen/SlotIndexes.h"
#include "llvm/CodeGen/VirtRegMap.h"

#include "llvm/PassAnalysisSupport.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Timer.h"

#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include "llvm/Target/TargetRegisterInfo.h"
#include <queue> 
#include <set>
#include <list>
#include <map>

using namespace llvm;
  class RegisterClassInfo;

  cl::opt<unsigned> NumPRFreedom("numprfree", cl::desc("manually set number of extra register"),
                                         cl::init(3));//seems 3 is necessary
    
  cl::opt<bool> SetNumRegs("setnumregs", cl::init(false),
                       cl::desc("only for testing purpose? setting num of phys regs(false)"));
  cl::opt<bool> NORPR("norpr", cl::init(false),
                       cl::desc("don't run register pressure releaser(false)"));

  #define DEBUG_TYPE "regalloc"
  //=======================Set up and stuff============================
  char RegisterPressureReleaser::ID = 0;
  char &llvm::RegisterPressureReleaserID = RegisterPressureReleaser::ID;
  INITIALIZE_PASS_BEGIN(RegisterPressureReleaser, "registerpressurereleaser",
                  "greedy spilling", false, false)
  INITIALIZE_PASS_DEPENDENCY(LiveIntervals)
  INITIALIZE_PASS_DEPENDENCY(MachineDominatorTree)
  INITIALIZE_PASS_DEPENDENCY(MachinePostDominatorTree)
  INITIALIZE_PASS_DEPENDENCY(NextAccessAnalyzer)
  INITIALIZE_PASS_END(RegisterPressureReleaser, "registerpressurereleaser",
                  "greedy spilling", false, false)
    
    
  void RegisterPressureReleaser::getAnalysisUsage(AnalysisUsage &AU) const {
    //AU.setPreservesCFG();
    AU.addRequired<MachineDominatorTree>();
    //AU.addPreserved<MachineDominatorTree>();
    AU.addRequired<MachinePostDominatorTree>();
    //AU.addPreserved<MachinePostDominatorTree>();
    AU.addRequired<SlotIndexes>();
    AU.addPreserved<SlotIndexes>();
    AU.addRequired<LiveIntervals>();
    AU.addPreserved<LiveIntervals>();
    AU.addRequired<LiveStacks>();
    AU.addPreserved<LiveStacks>();
    AU.addRequired<MachineLoopInfo>();
    //record spill info so that register allocators can spill later using rewriter
    AU.addRequired<VirtRegMap>();
    AU.addPreserved<VirtRegMap>();
    AU.addRequired<NextAccessAnalyzer>();
    MachineFunctionPass::getAnalysisUsage(AU);
  }

  void RegisterPressureReleaser::init(){
    FTR = true;
    maxRCW = 0;
  }


  RegisterPressureReleaser::RegisterPressureReleaser() : MachineFunctionPass(ID){
    init();
    initializeRegisterPressureReleaserPass(*PassRegistry::getPassRegistry());
  }

  RegisterPressureReleaser::~RegisterPressureReleaser(){
  }

  void RegisterPressureReleaser::releaseMemory(){
    loMap.clear();   //liveouts of bbs
    liMap.clear();   //liveins of bbs
    spills.clear();  //where the spills are
    restores.clear();//where the restores are
    slots.clear();   //where the vrs are spilled to
    ru2regs.clear(); //pr to vr in terms of reg units
    regDists.clear();//dists

    //data structure that holds vr, pr and dists together
    VIRT2INFOS::iterator it;
    for (it = virt2infos.begin(); it != virt2infos.end(); ++it){
      VIRT2INFO& virt2info = it->second;
      VIRT2INFO::iterator rit;
      for (rit = virt2info.begin(); rit != virt2info.end(); ++rit){
        RegInfo* ri = rit->second;
        delete ri;
      }
    }
    virt2infos.clear();
    fakeBranchesToErase.clear();
  }

  //=======================spills and restores============================
  //TODO: keep track of dominating spill BB's like I used to?
  MachineBasicBlock* RegisterPressureReleaser::FindSpillBlock(unsigned reg, MachineBasicBlock* MBB){
    return MBB;
  }

  void RegisterPressureReleaser::SpillRegAfter(unsigned reg, MachineInstr* spillLoc,
                                               const TargetRegisterClass* trc,
                                               unsigned slot){
    MachineBasicBlock::iterator miItr(spillLoc);
    MachineInstrSpan MIS(miItr);
    //insert spill after the def
    TII->storeRegToStackSlot(*spillLoc->getParent(), std::next(miItr), reg, false, slot, trc, TRI);
    LIS->InsertMachineInstrRangeInMaps(std::next(miItr), MIS.end());    
  }

  void RegisterPressureReleaser::SpillRegAt(unsigned spillReg, MachineInstr* spillLoc,
                                         const TargetRegisterClass* trc,
                                         unsigned slot){
    MachineBasicBlock::iterator miItr(spillLoc);
    MachineInstrSpan MIS(miItr);
    //insert spill before spillLoc
    TII->storeRegToStackSlot(*spillLoc->getParent(), miItr, spillReg, false, slot, trc, TRI);
    LIS->InsertMachineInstrRangeInMaps(MIS.begin(), miItr);

  }

  void RegisterPressureReleaser::RestoreRegAt(unsigned restoreReg, MachineInstr* restoreLoc,
                                         const TargetRegisterClass* trc,
                                         unsigned slot){
      MachineBasicBlock::iterator miItr(restoreLoc);
      MachineInstrSpan MIS(miItr);
      //before the first instruction ==
      //  after the beginning of restoreBB
      TII->loadRegFromStackSlot(*restoreLoc->getParent(), miItr, restoreReg, slot, trc, TRI);
      LIS->InsertMachineInstrRangeInMaps(MIS.begin(), miItr);

      MachineInstr* restoreInstr = --miItr;
      NAA->InsertAccess(restoreReg, restoreInstr, false);
  }

  //spill reg at instruction mi
  //side effect:
  //    1. live: loMap
  //    2. vr info
  // DON'T keep track of pressrue change locations since all spills comes with recoloring
  MachineInstr* RegisterPressureReleaser::SpillReg(unsigned spillReg, MachineInstr* mi, MachineBasicBlock* spillBlock){
    loMap[spillBlock].erase(spillReg);
    virt2infos[spillBlock][spillReg]->ClearPhys();

    //where reg was spilled
    SlotMap::iterator spillSlot = slots.find(spillReg);
    //definitely have not been spilled before
    if (spillSlot == slots.end()){
      //get a slot for this not yet spilled reg
      unsigned slot = VRM->assignVirt2StackSlot(spillReg);
      slots[spillReg] = slot;
      spilledRegs.insert(spillReg);
    }

    RegAccess* regAcc = NAA->GetLastAccess(spillReg, spillBlock);
    //spill at bb begin
    if (NULL == mi || regAcc == NULL){
      InsertSpillLocation(spillReg, NULL, spillBlock);
      //live in or not?
      //NOTE: no def at bb begin, so there is still room to push it upward
      // In cases where this is the location to insert the actual spill
      // Insert remaning spill and restores handle it
      spillIn[spillBlock].insert(spillReg);
      return NULL;
    }

    //mi != NULL
    SlotIndex curId = LIS->getInstructionIndex(mi);
    while(LIS->getInstructionIndex(regAcc->accInst) >= curId){
      regAcc = NAA->GetPrevAccess(spillReg, regAcc);
      if (regAcc == NULL){
        PushSpillUp(spillReg, spillBlock);
        //liMap[spillBlock].erase(spillReg);
        //InsertSpillLocation(spillReg, NULL, spillBlock);
        //spillIn[spillBlock].insert(spillReg);
        //MapPressurePoint(spillReg, spillBlock->rend(), spillBlock);
        //let's not record this in pressure change if not recolored
        //  the ru's for this value will be accounted for correctly
        return NULL;
      }
    }

    //spill(mark as pressure boundary) at the nearest def
    InsertSpillLocation(spillReg, regAcc->accInst, spillBlock);
    //we shouldn't record this because all spills seem to involve recoloring once right now
    //TODO: make recoloring location better
    //MapPressurePoint(spillReg, MBBRI(regAcc->accInst), spillBlock);

    //has to be spilled before restore. check for spill info
    if ((spillIn.find(spillBlock) == spillIn.end() ||
         spillIn[spillBlock].find(spillReg) == spillIn[spillBlock].end()) &&
        (spillKill.find(spillBlock) == spillKill.end() ||
         spillKill[spillBlock].find(spillReg) == spillKill[spillBlock].end())){
      MachineInstr* regAcc = NAA->GetFirstDef(spillReg, spillBlock);
      if (regAcc == NULL){
        spillIn[spillBlock].insert(spillReg);
        return NULL;
      }
      
      SlotIndex curId = LIS->getInstructionIndex(mi);
      if(LIS->getInstructionIndex(regAcc) < curId){
        spillKill[spillBlock].insert(spillReg);
      }
    }

    //return where the spill is inserted
    return regAcc->accInst;
  }

  bool RegisterPressureReleaser::CanPushToPred(unsigned reg, unsigned loopDepth, MachineBasicBlock* pred){
    unsigned predDepth = Loops->getLoopDepth(pred);
    if (predDepth < loopDepth)
      return true;
    else{
      //spilled already
      if (loMap.find(pred) != loMap.end() && loMap[pred].find(reg) != loMap[pred].end())
        return true;

      unsigned numValidSuccs = 0;
      //not spilled or not visited yet
      for (MachineBasicBlock::succ_iterator SI = pred->succ_begin(), E = pred->succ_end(); SI != E; ++SI) {
        MachineBasicBlock* succBB = *SI;
        unsigned succDepth = Loops->getLoopDepth(succBB);
        if (succDepth < predDepth)
          continue;

        //not spillIn for succBB, either not spilled in succ or succ has not been vistied yet
        if (spillIn.find(succBB) == spillIn.end() || spillIn[succBB].find(reg) == spillIn[succBB].end()){
          //simply not spilled in one of succBB of pred
          if (loMap.find(succBB) != loMap.end()){
            numValidSuccs++;
          }
          //else not vistied yet, let's say we can spill?
          //TODO: check if this else makes sense
        }
        //else spillin for succBB
      }

      return (numValidSuccs <= 1);
    }
  }

  //try push spill upward during allocation
  void RegisterPressureReleaser::PushSpillUp(unsigned reg, MachineBasicBlock* mbb){
    std::queue<MachineBasicBlock*> workList;  //list with which to proceed
    workList.push(mbb);
    std::set<MachineBasicBlock*> visited;
    visited.insert(mbb);

    while(!workList.empty()){
      MachineBasicBlock* curBB = workList.front();
      workList.pop();
      unsigned curDepth = Loops->getLoopDepth(mbb);
      bool allGood = true;
      for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(), E = curBB->pred_end(); PI != E; ++PI) {
        MachineBasicBlock* predBB = *PI;
        if (!CanPushToPred(reg, curDepth, predBB)){
          allGood = false;
          break;
        }
      }

      if (!allGood){
        InsertSpillLocation(reg, NULL, curBB);
        spillIn[curBB].insert(reg);
        continue;
      }

      if (liMap.find(curBB) != liMap.end())
        liMap[curBB].erase(reg);

      //all preds that are of the same loop depth or higher have no problem with mbb pushing spill into them
      for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(), E = curBB->pred_end(); PI != E; ++PI) {
        MachineBasicBlock* predBB = *PI;
        //not spilled in predBB
        if (spillOut.find(predBB) == spillOut.end() || spillOut[predBB].find(reg) == spillOut[predBB].end()){
          spillOut[predBB].insert(reg);
          if (loMap.find(predBB) != loMap.end())
            loMap[predBB].erase(reg);

          MachineInstr* lastDef = NAA->GetLastDef(reg, predBB);
          if (lastDef == NULL && visited.find(predBB) == visited.end()){
            workList.push(predBB);
          }
          else{
            InsertSpillLocation(reg, lastDef, predBB);
          }
        }
        //else checked already, don't add to list
      }
    }
  }

  //getting the spilled reg helps to push pressure change pointand thus the restore up
  MachineInstr* RegisterPressureReleaser::Spill(MachineInstr* mi, const TargetRegisterClass* RC, MachineBasicBlock* spillBlock){

    assert(regDists.find(spillBlock) != regDists.end() && "must have virt regs occupying units to spill");
    RegDists& dists = regDists[spillBlock];
    RegDists::iterator furthest = dists.begin();
    assert(furthest != dists.end()  && "can't find allocatable register holding a vir reg");
    RegInfo* rdist = *furthest;
    unsigned spillReg = rdist->virt;

    //does spilling this reg helps with the register pressure?
    while(!ShareRegUnits(RC, MRI->getRegClass(spillReg))){
      //check next furthest
      ++furthest;
      assert(furthest != dists.end() && "can't find allocatable register holding a vir reg");
      rdist = *furthest;
      spillReg = rdist->virt;
    }

    //now we have a value that helps with register pressure after spilling
    //spill reg should handle curlo, ru2regs, vr2pr
    MachineInstr* spillLoc = SpillReg(spillReg, mi, spillBlock);

    //not live anymore, affecting:
    //regDists update
    regDists[spillBlock].erase(furthest);
    return spillLoc;

  }

  void RegisterPressureReleaser::Restore(unsigned reg, MachineInstr* mi){
    assert (TRI->isVirtualRegister(reg) && "should restore only virtual registers");
    //TODO check is spill dominates
    assert (slots.find(reg) != slots.end() && "should spill before restore");

    MachineBasicBlock* mbb = mi->getParent();
    assert(mbb != NULL && "probably from an empty block");
    CurLiveRegs& curlo = loMap[mbb];

    //if reg is not in curlo, there is a good chance it doesn't have reg info for this block
    VIRT2INFO& virt2info = virt2infos[mbb];
    if (virt2info.find(reg) == virt2info.end()){
      virt2info[reg] = new RegInfo(reg, 0, 0);
    }

    //try to find a reg that can hold this reg, if not, spilling is needed.
    const TargetRegisterClass* highPressureClass = TryAssign(reg, mbb);
    if (highPressureClass != NULL){
        MachineInstr* spillLoc = Spill(mi, highPressureClass, mbb);
        Recolor(curlo, mbb);
        if (mi == mbb->begin())
          recolorLoc[mbb] = NULL;
        else if (spillLoc != NULL)
          recolorLoc[mbb] = spillLoc;//&*std::next(MachineBasicBlock::reverse_iterator(mi));
    }

    MCRegUnitIterator units(virt2info[reg]->phys, TRI);
    unsigned ru = *units;
    MachineInstr* insertLocation;

    //so pressure is lower than threshold from bb begin
    if (pressureChangePoints.find(mbb) == pressureChangePoints.end()){
      if (mightUsedRU.find(ru) == mightUsedRU.end())
        insertLocation = NULL;
      else
        insertLocation = recolorLoc[mbb];
    }
    else{
      RU2LOC& localPCP = pressureChangePoints[mbb];
      RU2LOC::iterator ruit = localPCP.find(ru);
      if (ruit == localPCP.end()){
        if (mightUsedRU.find(ru) == mightUsedRU.end())
          insertLocation = NULL;
        else
          insertLocation = recolorLoc[mbb];        
      }
      else
        insertLocation = ruit->second;

      InsertRestoreLocation(reg, insertLocation, mbb);
    }
    curlo.insert(reg);

    //has to be spilled before restore. check for spill info
    if ((spillIn.find(mbb) == spillIn.end() || spillIn[mbb].find(reg) == spillIn[mbb].end()) &&
        (spillKill.find(mbb) == spillKill.end() || spillKill[mbb].find(reg) == spillKill[mbb].end())){
      MachineInstr* regAcc = NAA->GetFirstDef(reg, mbb);
      if (regAcc == NULL){
        spillIn[mbb].insert(reg);
        return;
      }
      
      SlotIndex curId = LIS->getInstructionIndex(mi);
      if(LIS->getInstructionIndex(regAcc) < curId){
        spillKill[mbb].insert(reg);
        return;
      }
    }
  }

  //get actual spill/restore instruction inserted
  MachineInstr* RegisterPressureReleaser::GetInsertedInstr(MachineBasicBlock* mbb,
                                                           MachineInstr* insertLoc,
                                                           unsigned reg, bool isSpill){
    //before the first instr of original program, go down from first instr
    if (insertLoc == NULL){
      MachineBasicBlock::iterator mitr = mbb->begin();
      do{
        ++mitr;
      }while(!AccessReg(reg, mitr));
      return mitr;
    }

    //restore at use, go up from use
    if (!isSpill && AccessReg(reg, insertLoc)){
      MachineBasicBlock::iterator mitr(insertLoc);
      MachineInstr* resLoc = NULL;
      do{
        --mitr;
        resLoc = mitr;
      }while(!AccessReg(reg, resLoc));
      return resLoc;
    }

    //restore after spill or spill after def
    MachineBasicBlock::iterator mitr = MachineBasicBlock::iterator(insertLoc);
    do{
      ++mitr;
    }while(!AccessReg(reg, mitr));
    return mitr;
  }

  void RegisterPressureReleaser::AddLiveStackIntervals(){
    /*create live stack intervals*/
    SlotMap::iterator si;
    std::set<unsigned> spilledRegs;
    for (si = slots.begin(); si != slots.end(); ++si){
      unsigned spillReg = si->first;
      int slot = si->second;
      LiveInterval* newStackInterval = &LSS->getOrCreateInterval(slot, MRI->getRegClass(spillReg));
      newStackInterval->getNextValue(SlotIndex(), LSS->getVNInfoAllocator());
      //newStackInterval->MergeSegmentsInAsValue(LIS->getInterval(spillReg), newStackInterval->getValNumInfo(0));
      spilledRegs.insert(spillReg);
      stackInts[spillReg] = newStackInterval;
    }

    /*add within bb segments*/
    for (MachineFunction::iterator MBBI = MF->begin(), E = MF->end(); MBBI != E; ++MBBI){
      MachineBasicBlock* curBB = MBBI;
      if (spills.find(curBB) == spills.end())
        continue;

      SlotIndex bbEnd = LIS->getMBBEndIdx(curBB);
      SlotIndex bbBegin = LIS->getMBBStartIdx(curBB);

      SpillInstrs& bbSpills = spills[curBB];
      SpillInstrs::iterator it;
      std::set<unsigned>& curSpillIn = spillIn[curBB];
      std::set<unsigned>& curSpillKill = spillKill[curBB];
      std::set<unsigned>& curSpillOut = spillOut[curBB];
      SpillInstrs& curActualSpill = actualSpills[curBB];
      std::set<unsigned>::iterator srit;
      SlotIndex segBegin, segEnd;
      for (srit = spilledRegs.begin(); srit != spilledRegs.end(); ++srit){
        unsigned spillReg  = *srit;
        LiveInterval* stackInt = stackInts[spillReg];
        VNInfo* VN = stackInt->getValNumInfo(0);

        if (curSpillIn.find(spillReg) != curSpillIn.end()){
          MachineInstr* firstDef = NAA->GetFirstDef(spillReg, curBB);
          //live through curBB, add the whole bb to live interval and go to next reg
          if (firstDef == NULL){
            LiveRange::Segment S(
               bbBegin,
               bbEnd,
               VN);
            stackInt->addSegment(S);
            continue;
          }
          else{
            LiveRange::Segment S(
               bbBegin,
               LIS->getInstructionIndex(firstDef),
               VN);
            stackInt->addSegment(S);            
          }
        }

        //no live segments after def(kill)
        if (curActualSpill.find(spillReg) == curActualSpill.end())
          continue;

        // now add segments that starts within bb
        //current acutally spilled reg locations
        std::vector<MachineInstr*>& cassLoc = curActualSpill[spillReg];
        std::vector<MachineInstr*>::iterator it;
        MachineInstr* regDef = NAA->GetFirstDef(spillReg, curBB);
        for (it = cassLoc.begin(); it != cassLoc.end(); ++it){
          MachineInstr* insertLoc = GetInsertedInstr(curBB, *it, spillReg, true);
          while (regDef && LIS->getInstructionIndex(regDef) < LIS->getInstructionIndex(insertLoc)){
            insertLoc = NAA->GetNextDef(spillReg, regDef);
          }

          //no kill so add the whole bb
          if (regDef == NULL){
            LiveRange::Segment S(
               LIS->getInstructionIndex(insertLoc),
               bbEnd,
               VN);
            stackInt->addSegment(S);
            break;
          }//add up to the def, then check for next spill location
          else{
            LiveRange::Segment S(
               LIS->getInstructionIndex(insertLoc),
               LIS->getInstructionIndex(regDef),
               VN);
            stackInt->addSegment(S);
          }
        }
      }//for regs
    }//for bb
  }

  bool RegisterPressureReleaser::AccessReg(unsigned reg, MachineInstr* MI){
    for (unsigned i = 0, e = MI->getNumOperands(); i != e; ++i) {
      MachineOperand* Oper = &MI->getOperand(i);
      if (Oper->isReg() && Oper->getReg() == reg)
        return true;
    }
    return false;
  }

  void RegisterPressureReleaser::AddSpillInstructions(){
    /*spills first*/
    SpillMap::iterator sit;
    //spills for pressure releasing
    for (sit = spills.begin(); sit != spills.end(); ++sit){
      SpillInstrs& spillsInBB = sit->second;
      MachineBasicBlock* spillBB = sit->first;
      SpillInstrs::iterator it;
      for (it = spillsInBB.begin(); it != spillsInBB.end(); ++it){
        unsigned spillReg = it->first;
        assert(slots.find(spillReg) != slots.end() && "didn't assign a slot for spilled reg");
    
        const TargetRegisterClass *trc = MRI->getRegClass(spillReg);
        std::vector<MachineInstr*>& spillLocs = it->second;
        unsigned slot = slots[spillReg];
    
        for (unsigned i = 0; i < spillLocs.size(); i++){
          MachineInstr* spillLoc = spillLocs[i];
          if (spillLoc == NULL){
            if (spillBB->begin() == spillBB->end()){
              spillLoc = MF->CloneMachineInstr(MF->begin()->begin());
              spillBB->insert(spillBB->end(), spillLoc);
              fakeBranchesToErase.push_back(spillLoc);
            }
            else
              spillLoc = spillBB->begin();
          }
          else
            spillLoc = spillLocs[i];
          //spillLocs.pop_back();
          if (AccessReg(spillReg, spillLoc))
            SpillRegAfter(spillReg, spillLoc, trc, slot);
          else
            SpillRegAt(spillReg, spillLoc, trc, slot);
        }
      }
      //spillsInBB.clear();
    }
    //spills.clear();
  }

  void RegisterPressureReleaser::AddRestoreInstructions(){
    /*restores*/
    //restore when needed
    SpillMap::iterator sit;
    for (sit = restores.begin(); sit != restores.end(); ++sit){
      SpillInstrs& restoresInBB = sit->second;
      MachineBasicBlock* restoreBB = sit->first;
      SpillInstrs::iterator it;
      for (it = restoresInBB.begin(); it != restoresInBB.end(); ++it){
        unsigned restoreReg = it->first;
        const TargetRegisterClass *trc = MRI->getRegClass(restoreReg);

        std::vector<MachineInstr*>& restoreLocs = it->second;
        unsigned ss = slots[restoreReg];

        for (unsigned i = 0; i < restoreLocs.size(); i++){
          MachineInstr* mi = restoreLocs[i];
          if (mi == NULL){
            if (restoreBB->begin() == restoreBB->end()){
              mi = MF->CloneMachineInstr(MF->begin()->begin());
              restoreBB->insert(restoreBB->end(), mi);
              fakeBranchesToErase.push_back(mi);
            }
            else
              mi = restoreBB->begin();

            RestoreRegAt(restoreReg, mi, trc, ss);
          }
          else{
            if (AccessReg(restoreReg, mi))
              RestoreRegAt(restoreReg, mi, trc, ss);
            else{
              MachineBasicBlock::iterator miItr(mi);
              ++miItr;
              if (miItr == restoreBB->end()){
                MachineInstr* newLoc = MF->CloneMachineInstr(MF->begin()->begin());
                restoreBB->insert(restoreBB->end(), newLoc);
                fakeBranchesToErase.push_back(newLoc);
                RestoreRegAt(restoreReg, newLoc, trc, ss);
              }
              else{
                RestoreRegAt(restoreReg, miItr, trc, ss);
              }

            }
          }
        }
      }
      //restoresInBB.clear();
    }
    //restores.clear();
  }

  /*doing this at the end of traversal of a block to avoid changing the distances within a block*/
  void RegisterPressureReleaser::AddLoadAndStores(){
    AddRestoreInstructions();
    AddSpillInstructions();

    //clear up some useless bb and instructions
    for (unsigned i = 0; i < fakeBranchesToErase.size(); i++){
      MachineInstr* mi = fakeBranchesToErase[i];
      mi->eraseFromParent();
    }

    //delete the useless split mbb
    for (unsigned i = 0; i < blocksToErase.size(); i++){
      MachineBasicBlock* mbb = blocksToErase[i];
      MachineBasicBlock* pred = *mbb->pred_begin();
      MachineBasicBlock* succ = *mbb->succ_begin();
      pred->replaceSuccessor(mbb, succ);
      mbb->eraseFromParent();
    }
  }

  void RegisterPressureReleaser::PrepBBBegin(MachineBasicBlock* curBB){
    std::map<unsigned, unsigned>&ru2reg =  ru2regs[curBB];
    recolorLoc[curBB] = NULL;
  
    livePhysRegs.clear();
    //add physreg livein, probably just for function entry
    MachineBasicBlock::livein_iterator lit;
    for ( lit = curBB->livein_begin(); lit!= curBB->livein_end(); ++lit){
      unsigned physReg = *lit;
      livePhysRegs.insert(physReg);
      for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
        ru2reg.erase(*Units);
      }
    }
  
    RegDists newDists(MF);
    regDists[curBB] = newDists;
  
#ifndef RPR_DEBUG
    CurLiveRegs newli(SimpleRegCompare(MF));
    CurLiveRegs newlo(SimpleRegCompare(MF));
#else
    CurLiveRegs newli(SimpleRegCompare(MF, &regOrders));
    CurLiveRegs newlo(SimpleRegCompare(MF, &regOrders));
#endif
  
    liMap[curBB] = newli;
    CurLiveRegs& curli = liMap[curBB];//merge from predeseccors' live outs
  
    loMap[curBB] = newlo;
    CurLiveRegs& curlo = loMap[curBB];
  
    /*merge lives from predecessors*/
    for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(), E = curBB->pred_end(); PI != E; ++PI) {
      MachineBasicBlock* predBB = *PI;
      CurLiveMap::iterator it = loMap.find(predBB);
      if (it != loMap.end()){
        //self looping should be handled at BB end
        //TODO, be aware of the looping and make better spill choices
        if (predBB == curBB){
            continue;
        }
        const CurLiveRegs& predLive= it->second;
        curlo.insert(predLive.begin(), predLive.end());
      }
    }
  
    //result from spiliting or something
    if (curBB->empty()){
      curli.insert(curlo.begin(), curlo.end());
      return;
    }
  
    /*set up next distance for spilling*/
    //TODO add weight measurement
    SmallVector<unsigned, 16> deadLiveIn;
    for (CurLiveRegs::iterator it = curlo.begin(); it != curlo.end(); ++it){
      unsigned reg = *it;
      MachineBasicBlock* spillBlock = FindSpillBlock(reg, curBB);
      RegAccess* acc = NAA->GetFirstAccess(reg, curBB);
      unsigned dist;
      if (!acc){
        if (spillBlock)
          dist = NAA->GetDistance(reg, curBB->begin(), spillBlock);
        else
          dist = NAA->GetDistance(reg, curBB->begin(), curBB);
      }
      else{
          SlotIndex bbsi = LIS->getMBBStartIdx(curBB);
          SlotIndex asi = LIS->getInstructionIndex(acc->accInst);
          //could be zero, it's fine
          dist = bbsi.distance(asi);//dist within mbb
      }
  
      //regs with infinity dists are dead at this point
      //NOTE: they could be live in other paths and that's why they are passed down here
      if (dist != INFINITY_DIST){
        RegInfo* ri = new RegInfo(reg, 0, dist);
        virt2infos[curBB][reg] = ri;
        regDists[curBB].insert(ri);
      }
      else
        deadLiveIn.push_back(reg);
    }
  
    for (unsigned i = 0; i < deadLiveIn.size(); i++){
      curlo.erase(deadLiveIn[i]);
    }

    while(true){
      const TargetRegisterClass* RC = TryColor(curBB);
      //we have a set of values (virts and phys) that fit in phys regs
      if (RC == NULL)
        break;

      Spill(NULL, RC, curBB);
    }
  
    //only for unvisited pred bb
    curli.insert(curlo.begin(), curlo.end());

    RU2LOC& localPCP = pressureChangePoints[curBB];//pressure change points in this bb
    //for marking before the first instruction
    //  all other locations have actual instructions
    //for (unsigned i = 0; i < TRI->getNumRegUnits(); i++){
    //  if (ru2reg.find(i) == ru2reg.end()){
    //    //this means that any restore to register unit i can be added at this point
    //    localPCP[i] = bbBegin;
    //  }
    //}
  }

  void RegisterPressureReleaser::DealWithBBEdges(){
    std::map<unsigned, std::vector<MachineBasicBlock*>> MBBS;
    std::map<unsigned, std::vector<MachineBasicBlock*>>::reverse_iterator depthIt;

    for (MachineFunction::iterator MBBI = MF->begin(), E = MF->end(); MBBI != E; ++MBBI){
      MachineBasicBlock* curBB = MBBI;
      MBBS[Loops->getLoopDepth(curBB)].push_back(curBB);
    }

    for (depthIt = MBBS.rbegin(); depthIt != MBBS.rend(); ++depthIt){
      unsigned curDepth = depthIt->first;
      std::vector<MachineBasicBlock*>& mbbs = depthIt->second;
      std::vector<MachineBasicBlock*>::iterator mit;
      for (mit = mbbs.begin(); mit != mbbs.end(); ++mit){
        MachineBasicBlock* curBB = *mit;
        if (curBB->succ_size() == 0)
          continue;

        CurLiveRegs capSucc(SimpleRegCompare(MF, &regOrders));
        CurLiveRegs tmp(SimpleRegCompare(MF, &regOrders));

        //std::set<unsigned> capSucc, tmp;
        MachineBasicBlock::succ_iterator SI, E;

        /*--------add spill and restore within bb--------*/
        for (SI = curBB->succ_begin(), E = curBB->succ_end(); SI != E; ++SI) {
          MachineBasicBlock* succ = *SI;
          if (Loops->getLoopDepth(succ) < curDepth)
            continue;
          if (liMap.find(succ) == liMap.end() || liMap[succ].size() == 0)
            continue;
          capSucc.insert(liMap[succ].begin(), liMap[succ].end());
          ++SI;
          break;
        }

        for (; SI != E; ++SI){
          MachineBasicBlock* succ = *SI;
          std::set_intersection(capSucc.begin(), capSucc.end(),
                    liMap[succ].begin(), liMap[succ].end(),
                    std::inserter(tmp, tmp.begin()),
                    SimpleRegCompare(MF, &regOrders));
          capSucc.clear();
          capSucc.insert(tmp.begin(), tmp.end());
          tmp.clear();
        }

        CurLiveRegs& curLo = loMap[curBB];
        //live out - cap live in's: to spill
        std::set_difference(curLo.begin(), curLo.end(),
                            capSucc.begin(), capSucc.end(),
                            std::inserter(tmp, tmp.begin()),
                            SimpleRegCompare(MF, &regOrders));

        for (std::set<unsigned>::iterator it = tmp.begin(); it != tmp.end(); ++it){
          unsigned reg = *it;
          if (spillOut[curBB].find(reg) == spillOut[curBB].end()){
            loMap[curBB].erase(reg);
            spillOut[curBB].insert(reg);
            MachineInstr* lastDef = NAA->GetLastDef(reg, curBB);
            //TODO: push up?
            //if (lastDef == NULL)
              InsertSpillLocation(reg, lastDef, curBB);
          }
        }

        //cap live in's - live out: to restore
        tmp.clear();
        std::set_difference(capSucc.begin(), capSucc.end(),
                            curLo.begin(), curLo.end(),
                            std::inserter(tmp, tmp.begin()),
                            SimpleRegCompare(MF, &regOrders));
        //where to add restore
        if (tmp.size() != 0){
          MachineInstr* firstBranch = &*(++MachineBasicBlock::reverse_iterator(curBB->getFirstTerminator()));
          if (firstBranch == NULL)
            firstBranch = &*curBB->rbegin();
  
          for (std::set<unsigned>::iterator it = tmp.begin(); it != tmp.end(); ++it){
            unsigned reg = *it;
            loMap[curBB].insert(reg);
            InsertRestoreLocation(reg, firstBranch, curBB);
          }
        }

        /*--------add spill and restore on bb edge--------*/
        for (SI = curBB->succ_begin(); SI != E; ++SI) {
          MachineBasicBlock* succ = *SI;
          SpillAndRestoreAtBBEdge(curBB, succ);
        }
      }
    }
  }
  
  //This function does:
  //  add restore at pred->succ bb edge to match succ's live in
  //  extra live regs in live out of pred will be handled by AddExtraSpills()
  void RegisterPressureReleaser::SpillAndRestoreAtBBEdge(MachineBasicBlock* pred,
                                                         MachineBasicBlock* succ){
    //check the difference between live out of pred and live in of succ
    CurLiveRegs& predlo = loMap[pred];
    CurLiveRegs& succli = liMap[succ];
  #ifndef RPR_DEBUG
    CurLiveRegs restoreSet(SimpleRegCompare(MF));
  
    //succli - predlo
    std::set_difference(succli.begin(),  succli.end(),
                       predlo.begin(), predlo.end(),
                       std::inserter(restoreSet, restoreSet.begin()),
                       SimpleRegCompare(MF));
  #else
    CurLiveRegs restoreSet(SimpleRegCompare(MF, &regOrders));
  
    //succli - predlo
    std::set_difference(succli.begin(),  succli.end(),
                       predlo.begin(), predlo.end(),
                       std::inserter(restoreSet, restoreSet.begin()),
                       SimpleRegCompare(MF, &regOrders));
  #endif
  
    if (restoreSet.size()){
      MachineBasicBlock* branchingBB;
  
      //MachineBasicBlock::succ_iterator si = pred->succ_begin();
      //skip self looping
      //if (*si == pred)
      //  ++si;
      //++si;
      //skip self looping
      //if (*si == pred)
      //  ++si;
  
      //pred has only one succ that's not itself
      //if (si == pred->succ_end())
      if (pred->succ_size() == 1)
        branchingBB = pred;
      else
        branchingBB = pred->SplitCriticalEdge(succ, this);
  
      MachineInstr* branch = GetTerminator(branchingBB, succ);
      if( !branch){
        //something we can use to add spill and restore
        //branch = MF->CreateMachineInstr(TII->get(0), branchingBB->findDebugLoc(branchingBB->end()));
        branch = MF->CloneMachineInstr(succ->begin());
        branchingBB->insert(branchingBB->end(), branch);
        fakeBranchesToErase.push_back(branch);
      }
                        
      if (branch == branchingBB->begin())
        branch = NULL;
      //else
      //  branch = &*(std::next(MachineBasicBlock::reverse_iterator(branch)));

      CurLiveRegs::iterator rit;
      for (rit = restoreSet.begin(); rit != restoreSet.end(); ++rit){
        if (FindSpillBlock(*rit, pred)){
              InsertRestoreLocation(*rit, branch, branchingBB);
        }
      }
        
    }
  }

  //assuming few number of restoers per register per block
  void RegisterPressureReleaser::InsertRestoreLocation(unsigned reg,
                                                       MachineInstr* location,
                                                       MachineBasicBlock* mbb){
    std::vector<MachineInstr*>& restoreLocs = restores[mbb][reg];
    if (location == NULL){
      //shouldn't be like this
      if (restoreLocs.size() && restoreLocs[0] == NULL){
        //assert(false && "restore multiple times at bbBegin");
        return;
      }

      restoreLocs.insert(restoreLocs.begin(), location);
      return;
    }

    std::vector<MachineInstr*>::iterator it;
    for (it = restoreLocs.begin(); it != restoreLocs.end(); ++it){
      MachineInstr* mi = *it;
      if (mi == NULL)
        continue;

      if(mi == location){
        //assert(false && "restore multiple times at the same location");
        return;
      }
      if (LIS->getInstructionIndex(mi) > LIS->getInstructionIndex(location)){
        break;
      }
    }

    if (it == restoreLocs.end())
      restoreLocs.push_back(location);
    else
      restoreLocs.insert(it, location);

  }

  void RegisterPressureReleaser::MapPressurePoint(unsigned reg, MachineInstr* location, MachineBasicBlock* mbb){
    unsigned physReg = virt2infos[mbb][reg]->phys;
    RU2LOC& localPCP = pressureChangePoints[mbb];
    for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
      localPCP[*Units] = location;
    }
  }


  void RegisterPressureReleaser::InsertSpillLocation(unsigned reg,
                                                     MachineInstr* location,
                                                     MachineBasicBlock* mbb){
    SlotMap::iterator spillSlot = slots.find(reg);
    //definitely have not been spilled before
    if (spillSlot == slots.end()){
      //get a slot for this not yet spilled reg
      unsigned slot = VRM->assignVirt2StackSlot(reg);
      slots[reg] = slot;
      spilledRegs.insert(reg);
    }

    std::vector<MachineInstr*>& spillLocs = spills[mbb][reg];

    if (location == NULL){
      if (spillLocs.size() == 0){
        spillLocs.push_back(location);
        return;
      }

      std::vector<MachineInstr*>::iterator it = spillLocs.begin();
      assert(*it != location && "spilling the same thing twice");
//      if (*it != location)
        spillLocs.insert(it, location);
      //else spillLocs[0] == location
      return;
    }

    std::vector<MachineInstr*>::iterator it;
    for (it = spillLocs.begin(); it != spillLocs.end(); ++it){
      MachineInstr* mi = *it;
      if (mi == NULL)
        continue;

      if (LIS->getInstructionIndex(mi) >= LIS->getInstructionIndex(location)){
        if (mi == location)
          return;
        break;
      }
    }

    if (it != spillLocs.end())
      spillLocs.insert(it, location);
    else
      spillLocs.push_back(location);
  }

  void RegisterPressureReleaser::PropSpillInUpWard(){
    std::vector<MachineBasicBlock*> workOrder;//order to be based on
    std::queue<MachineBasicBlock*> workList;  //list with which to proceed
    std::set<MachineBasicBlock*> workSet;     //bbs need proceccing
    
    MachineBasicBlock* entry = MF->begin();
    for (po_iterator<MachineBasicBlock*> it = po_begin(entry),
         e = po_end(entry); it != e; ++it) {
      workOrder.push_back(*it);
      workSet.insert(*it);
    }

    while(!workSet.empty()){
      std::vector<MachineBasicBlock*>::iterator it;
      for (it = workOrder.begin(); it != workOrder.end(); ++it){
        if (workSet.find(*it)!= workSet.end())
          workList.push(*it);
      }
      workSet.clear();
    
      while(!workList.empty()){
        MachineBasicBlock* curBB = workList.front();
        workList.pop();
        
        //curBB was added by one of the succ bb. it sees the change now
        workSet.erase(curBB);

        //spillIn in all succs
        std::set<unsigned> capSucc, tmp;
        if (curBB->succ_size() != 0){
          capSucc.insert(spilledRegs.begin(), spilledRegs.end());
          for (MachineBasicBlock::succ_iterator SI = curBB->succ_begin(),
                                                 E = curBB->succ_end(); SI != E; ++SI) {
            MachineBasicBlock* succ = *SI;
            //some succ doesn't have spillIn
            if (spillIn.find(succ) == spillIn.end()){
              capSucc.clear();
              break;
            }
  
            std::set_intersection(capSucc.begin(), capSucc.end(),
                                spillIn[succ].begin(), spillIn[succ].end(),
                                std::inserter(tmp, tmp.begin()));
            capSucc.clear();
            capSucc.insert(tmp.begin(), tmp.end());
            tmp.clear();
          }//get reg intersection
        }

        std::set<unsigned>::iterator it;
        for (it = capSucc.begin(); it != capSucc.end(); ++it){
          unsigned reg = *it;
          if (spillOut.find(curBB) == spillOut.end() || spillOut[curBB].find(reg) == spillOut[curBB].end()){
            spillOut[curBB].insert(reg);
            MachineInstr* lastDef = NAA->GetLastDef(reg, curBB);
            //killed at def
            if (lastDef != NULL){
              spillKill[curBB].insert(reg);
              InsertSpillLocation(reg, lastDef, curBB);
            }
            else if(spillIn.find(curBB) == spillIn.end()||
                    spillIn[curBB].find(reg) == spillIn[curBB].end()){
              spillIn[curBB].insert(reg);

              //now curBB changed, add pred BB's to work set
              for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(),
                                                     E = curBB->pred_end(); PI != E; ++PI) {
                workSet.insert(*PI);
              }
            }
          }//if need updating
        }//check regs in intersection

      }//iteration
    }//still something to work on
  }

  void RegisterPressureReleaser::PropSpillOutDownWard(){
    std::vector<MachineBasicBlock*> workOrder;//order to be based on
    std::queue<MachineBasicBlock*> workList;  //list with which to proceed
    std::set<MachineBasicBlock*> workSet;     //bbs need proceccing
    
    MachineBasicBlock* entry = MF->begin();
    for (po_iterator<MachineBasicBlock*> it = po_begin(entry),
         e = po_end(entry); it != e; ++it) {
      workOrder.push_back(*it);
      workSet.insert(*it);
    }
    
    while(!workSet.empty()){
      std::vector<MachineBasicBlock*>::iterator it;
      for (it = workOrder.begin(); it != workOrder.end(); ++it){
        if (workSet.find(*it)!= workSet.end())
          workList.push(*it);
      }
      workSet.clear();
    
      while(!workList.empty()){
        MachineBasicBlock* curBB = workList.front();
        workList.pop();
        
        //curBB was added by one of the succ bb. it sees the change now
        workSet.erase(curBB);
    
        //spillOut in all preds
        std::set<unsigned> capPred, tmp;
        capPred.insert(spilledRegs.begin(), spilledRegs.end());
        for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(),
                                               E = curBB->pred_end(); PI != E; ++PI) {
          MachineBasicBlock* pred = *PI;
          //some succ doesn't predhave spillIn
          if (spillIn.find(pred) == spillIn.end()){
            capPred.clear();
            break;
          }
    
          std::set_intersection(capPred.begin(), capPred.end(),
                              spillIn[pred].begin(), spillIn[pred].end(),
                              std::inserter(tmp, tmp.begin()));
          capPred.clear();
          capPred.insert(tmp.begin(), tmp.end());
          tmp.clear();
        }//get reg intersection
    
        std::set<unsigned>::iterator it;
        for (it = capPred.begin(); it != capPred.end(); ++it){
          unsigned reg = *it;
          if (spillIn.find(curBB) == spillIn.end() || spillIn[curBB].find(reg) == spillIn[curBB].end()){
            spillIn[curBB].insert(reg);
            MachineInstr* firstDef = NAA->GetFirstDef(reg, curBB);
            //killed at def
            if (firstDef != NULL){
              spillKill[curBB].insert(reg);
            }
            else if(spillOut.find(curBB) == spillOut.end()||
                    spillOut[curBB].find(reg) == spillOut[curBB].end()){
              spillOut[curBB].insert(reg);
    
              //now curBB changed, add pred BB's to work set
              for (MachineBasicBlock::succ_iterator SI = curBB->succ_begin(),
                                                     E = curBB->succ_end(); SI != E; ++SI) {
                workSet.insert(*SI);
              }
            }
          }//if need updating
        }//check regs in intersection
    
      }//iteration
    }//still something to work on

  }

  void RegisterPressureReleaser::AddRemaningSpillsAndRestores(){
    PropSpillInUpWard();
    PropSpillOutDownWard();
    for (MachineFunction::iterator MBBI = MF->begin(), E = MF->end(); MBBI != E; ++MBBI){
      MachineBasicBlock* curBB = MBBI;

      //check for spills to add
      if (spillIn.find(curBB) != spillIn.end()){
        //spillOut in all preds
        std::set<unsigned> capPred, tmp;
        std::set<unsigned>& curSpillIn = spillIn[curBB];
        capPred.insert(spilledRegs.begin(), spilledRegs.end());
        for (MachineBasicBlock::pred_iterator PI = curBB->pred_begin(),
                                               E = curBB->pred_end(); PI != E; ++PI) {
          MachineBasicBlock* pred = *PI;
          //some succ doesn't predhave spillIn
          if (spillIn.find(pred) == spillIn.end()){
            capPred.clear();
            break;
          }
        
          std::set_intersection(capPred.begin(), capPred.end(),
                              spillIn[pred].begin(), spillIn[pred].end(),
                              std::inserter(tmp, tmp.begin()));
          capPred.clear();
          capPred.insert(tmp.begin(), tmp.end());
          tmp.clear();
        }//get reg intersection

        //what to spill: spillin - cap(pred spillin's)
        std::set<unsigned> needSpilling;
        std::set_difference(curSpillIn.begin(), curSpillIn.end(),
                            capPred.begin(), capPred.end(),
                            std::inserter(needSpilling, needSpilling.begin()));

        std::set<unsigned>::iterator it;
        SpillInstrs& localActualSpills = actualSpills[curBB];
        for (it = needSpilling.begin(); it != needSpilling.end(); ++it){
          unsigned reg = *it;
          std::vector<MachineInstr*>& regActualSpills = localActualSpills[reg];
          regActualSpills.insert(regActualSpills.begin(), NULL);
        }
      }

      //check for restores to add?
      
    }
  }

  //update live intervals of values with live range spilled or partially spilled
  void RegisterPressureReleaser::UpdateSpilledLiveRanges(){

    for (MachineFunction::iterator MBBI = MF->begin(), E = MF->end(); MBBI != E; ++MBBI){
      MachineBasicBlock* curBB = MBBI;
      CurLiveRegs& curli = liMap[curBB];
      SpillInstrs& curSpill = spills[curBB];
      SpillInstrs& curRestore = restores[curBB];
      SlotIndex bbEnd = LIS->getMBBEndIdx(curBB);
      SlotIndex bbBegin = LIS->getMBBStartIdx(curBB);

      std::set<unsigned>::iterator sit;
      for (sit = spilledRegs.begin(); sit != spilledRegs.end(); ++sit){
        unsigned reg = *sit;
        LiveInterval* li = &LIS->getInterval(reg);
        std::vector<MachineInstr*>::iterator sLoc;//spillLocation

        //================first deal with bb begin, live in================
        LiveRange::iterator lit = li->find(bbBegin);
        MachineInstr* regDef = NAA->GetFirstDef(reg, curBB);

        SlotIndex segStart;
        SlotIndex segEnd;

        //the new live range should be included in the old one
        if (lit != li->end()){//originally live in
          segStart = std::max(lit->start, bbBegin);
          segEnd = std::min(lit->end, bbEnd);
          
          if (segStart > segEnd)
            continue;
  
          //spilled before curBB
          if (curli.find(reg) == curli.end() && segStart == bbBegin){
              if (!regDef){
                li->removeSegment(bbBegin, segEnd);
                continue;
              }
              else{
                segStart = std::min(LIS->getInstructionIndex(regDef).getRegSlot(), segEnd);
                li->removeSegment(bbBegin, segStart);
                lit = li->find(segStart);
                VNInfo* newVNI = new (LIS->getVNInfoAllocator()) VNInfo(0, segStart);
                lit->valno = newVNI;
              }
          }
  
          //no spill in curbb
          if (curSpill.find(reg) == curSpill.end())
            continue;
        }

        //================now deal with spills in curBB================
        std::vector<MachineInstr*>& regSpill = curSpill[reg];
        std::vector<MachineInstr*>::iterator spit = regSpill.begin();
        for (spit = regSpill.begin(); spit != regSpill.end(); ++spit){
          MachineInstr* spillInstr;
          SlotIndex spillIdx = bbBegin;
          spillInstr = GetInsertedInstr(curBB, *spit, reg, true);
          if (spillInstr)
            spillIdx = LIS->getInstructionIndex(spillInstr);
          else
            spillIdx = bbBegin;
  
          lit = li->find(spillIdx);
          segStart = lit->start;
          segEnd   = lit->end;
  
          //no live range for this spill, weird.
          assert (lit != li->end() && "spilling out of range");
          //now remove (spill loc, next def/bb end)
          //now find a def or restore
          if (regDef == NULL){
            li->removeSegment(spillIdx, std::min(segEnd, bbEnd));
            break;
          }
          else{
            while(regDef != NULL && LIS->getInstructionIndex(regDef) < spillIdx){
              regDef = NAA->GetNextDef(reg, regDef);
            }
            if (regDef == NULL){
              li->removeSegment(spillIdx, std::min(segEnd, bbEnd));
              break;
            }
            SlotIndex defIdx = LIS->getInstructionIndex(regDef).getRegSlot();
            assert (defIdx <= segEnd && "def out of live range");

            li->removeSegment(spillIdx, defIdx);
            segStart = defIdx;
            lit = li->find(defIdx);
            //also mark it as reg use
            VNInfo* newVNI = new (LIS->getVNInfoAllocator()) VNInfo(0, defIdx);
            lit->valno = newVNI;
          }
        }
      }//for each spilled reg
    }//for bb

    std::set<unsigned>::iterator sit;
    for (sit = spilledRegs.begin(); sit != spilledRegs.end(); ++sit){
      unsigned reg = *sit;
      LiveInterval* li = &LIS->getInterval(reg);
      li->RenumberValues();
    }
  }

  bool RegisterPressureReleaser::ShareRegUnits(const TargetRegisterClass* RC1, const TargetRegisterClass* RC2){
    TargetRegisterClass::iterator it;
    for (it = RC1->begin(); it != RC1->end(); ++it){
      MCPhysReg physReg = *it;
      for(MCRegAliasIterator AI(physReg, TRI, true); AI.isValid(); ++AI){
        unsigned AliasReg = *AI;
        if (RC2->contains(AliasReg))
          return true;
      }
    }

    return false;
  }

  void RegisterPressureReleaser::ClearVR2PR(MachineBasicBlock* mbb){
    VIRT2INFO& virt2info = virt2infos[mbb];
    VIRT2INFO::iterator it;
    for (it = virt2info.begin(); it != virt2info.end(); ++it){
      it->second->ClearPhys();
    }
  }

  //recolor the list of operands
  //return the register class of the first mo that has problem coloring
  const TargetRegisterClass* RegisterPressureReleaser::Recolor(CurLiveRegs& regs,
                                                               MachineBasicBlock* mbb){
    std::map<unsigned, unsigned>& ru2reg = ru2regs[mbb];
    ru2reg.clear();
    ClearVR2PR(mbb);

    //deal with physregs first
    CurLivePhysRegs::iterator pit;
    for (pit = livePhysRegs.begin(); pit != livePhysRegs.end(); ++pit){
      unsigned physReg = *pit;
      
      //assuming no interference with other physical registers
      for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
        ru2reg[*Units] = physReg;
      }
    }

    //virtual registers
    CurLiveRegs::iterator it;    
    for (it = regs.begin(); it != regs.end(); ++it){
      unsigned reg = *it;
      // this is the first reg that gets trouble
      // all previous reg have higher priorities
      // spill something that free a unit it can use
      if (!TryAssignVirtOnce(reg, mbb))
        return MRI->getRegClass(reg);
    }

    RU2LOC& localPCP = pressureChangePoints[mbb];
    std::map<unsigned, unsigned>::iterator ruit;
    //whatever that's mapped is no longer available
    for (ruit = ru2reg.begin(); ruit != ru2reg.end(); ++ruit){
      unsigned RU = ruit->first;
      localPCP.erase(RU);
      //The end point can be at earliest recoloring points
      mightUsedRU.insert(RU);
    }

    return NULL;
  }

  //simply find free units for virt reg to use
  bool RegisterPressureReleaser::TryAssignVirtOnce(unsigned reg, MachineBasicBlock* mbb){
    const TargetRegisterClass* rc = MRI->getRegClass(reg);
    ArrayRef<MCPhysReg>& order = regOrders[rc];
    std::map<unsigned, unsigned>& ru2reg = ru2regs[mbb];
    VIRT2INFO& virt2info = virt2infos[mbb];

    assert(virt2info.find(reg) != virt2info.end() && "must have info ready before use");
    if (virt2info[reg]->phys != 0)
      return true;
    
    ArrayRef<MCPhysReg>::iterator it;
    for (it = order.begin(); it != order.end(); ++it){
      MCPhysReg physReg = *it;
      bool allUnitValid = true;
      for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
        unsigned unit = *Units;

        //if mapped, go for the next register
        if (ru2reg.find(unit) != ru2reg.end()){
          allUnitValid = false;
          break;
        }
      }
  
      //found a physReg that's available
      if (allUnitValid){
        for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
          unsigned unit = *Units;
          ru2reg[unit] = reg;
        }
        virt2info[reg]->SetPhys(physReg);
        return true;
      }
    }//try to find a physReg

    //failed
    return false;
  }

  bool RegisterPressureReleaser::TryAssignPhysReg(unsigned physReg, MachineBasicBlock* mbb){
    livePhysRegs.insert(physReg);
    std::map<unsigned, unsigned>& ru2reg = ru2regs[mbb];
    VIRT2INFO& virt2info = virt2infos[mbb];

    // for keeping track of virt regs occupying units we need
    SmallVector<unsigned, 4> otherVirts;
    unsigned lastVirt = 0;
    for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
      unsigned unit = *Units;      
      std::map<unsigned, unsigned>::iterator uit = ru2reg.find(unit);

      //this if means unit was mapped and it's not physreg
      if (uit != ru2reg.end() && (uit->second != physReg)){
        unsigned otherVirt = uit->second;
        assert(virt2info.find(otherVirt) != virt2info.end() && "must have info for other virt ready before use");
        if (lastVirt != otherVirt){
          otherVirts.push_back(otherVirt);
          virt2info[otherVirt]->ClearPhys();
          lastVirt = otherVirt;
        }
      }
      ru2reg[unit] = physReg;
    }
    
    if (!otherVirts.size())
      return true;

    //try recolor other mo once so that the split cost is still lower than spill
    SmallVector<unsigned, 4>::iterator it;
    for (it = otherVirts.begin(); it != otherVirts.end(); ++it){
      unsigned virt = *it;
      if (!TryAssignVirtOnce(virt, mbb))
        return false;
    }

    //other mo fit in easily
    return true;
  }

  //NOTE: opr may not be from mbb, using opr is sole for the reg class info
  /*simple assign, only assign to empty spots*/
  const TargetRegisterClass* RegisterPressureReleaser::TryAssign(unsigned reg,
                                                                 MachineBasicBlock* mbb){
    const TargetRegisterClass* rc = MRI->getRegClass(reg);
    const RegClassWeight& rcw = TRI->getRegClassWeight(rc);

    std::map<unsigned, unsigned>& ru2reg = ru2regs[mbb];
    VIRT2INFO& virt2info = virt2infos[mbb];

    //physical registers are precolored, recolor everything if reg not available
    if (TRI->isPhysicalRegister(reg)){
      if(TryAssignPhysReg(reg, mbb))
        return NULL;
      //failure goes to recoloring
    }//done with physical register
    else{
      //can find a physReg easily
      if (TryAssignVirtOnce(reg, mbb))
        return NULL;

      //can't find a physReg easily
      ArrayRef<MCPhysReg>& order = regOrders[rc];

      //need to recolor another value
      ArrayRef<MCPhysReg>::iterator it;
      for (it = order.begin(); it != order.end(); ++it){
        MCPhysReg physReg = *it;
        //candidate values to kick out
        // if find a color easily, return true
        // if not put them back and look for the next possible reg
        SmallVector<std::pair<unsigned, unsigned>, 4> toColor;

        //check if there might be enough units
        bool mightWork = true;
        unsigned lastReg = 0;
        for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
          unsigned unit = *Units;

          //iterator pointing to possible recolorable unit
          std::map<unsigned, unsigned>::iterator cit = ru2reg.find(unit);
          if ( cit == ru2reg.end()){
            ru2reg[unit] = reg;
            continue;
          }

          //some other value occupying this unit
          unsigned otherReg = cit->second;
          //virt reg cannot kick a phys reg out
          if (TRI->isPhysicalRegister(otherReg)){
            mightWork = false;
            break;//check next phys reg
          }
          const TargetRegisterClass* otherRC = MRI->getRegClass(otherReg);

          const RegClassWeight& otherRCW = TRI->getRegClassWeight(otherRC);
          //not a value we want to replace with reg
          if (rcw.WeightLimit > otherRCW.WeightLimit){
            //remove empty reg units (partial regs)
            for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
              unsigned unit = *Units;
            
              //iterator pointing to possible recolorable unit
              std::map<unsigned, unsigned>::iterator cit = ru2reg.find(unit);
              if ( cit != ru2reg.end() && ru2reg[unit] == reg){
                ru2reg.erase(cit);
              }
            }

            mightWork = false;
            break;//check next phys reg
          }
          else if (rcw.WeightLimit == otherRCW.WeightLimit && rcw.RegWeight >= otherRCW.RegWeight){
            mightWork = false;
            break;//check next phys reg
          }
          //at least one thing is smaller
          else{
            if (lastReg != otherReg){
              assert (virt2info.find(otherReg) != virt2info.end() && "must have info for otherReg ready before use");
              unsigned otherPR = virt2info[otherReg]->phys;
              toColor.push_back(std::make_pair(otherPR, otherReg));
              virt2info[otherReg]->ClearPhys();
              lastReg = otherReg;
            }
            ru2reg[unit] = reg;
          }
        }

        //try recolor other values in the current units
        if (!mightWork){
          continue;//go to next phys reg
        }
        else{
          unsigned i;
          for ( i = 0; i < toColor.size(); i++){
            unsigned otherReg = toColor[i].second;
            if (!TryAssignVirtOnce(otherReg, mbb)){
              break;
            }
          }
          //all recolored
          if(i == toColor.size()){
            virt2info[reg]->SetPhys(physReg);
            return NULL;
          }
        }
        
        //reassign not working, need to check the next possible register
        //clean up opr
        for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
          unsigned unit = *Units;
          
          std::map<unsigned, unsigned>::iterator cit = ru2reg.find(unit);
          if (cit != ru2reg.end() && cit->second == reg)
            ru2reg.erase(cit);
        }

        //put back modified other op mappings
        for (unsigned i = 0; i < toColor.size(); i++){
          unsigned origPR = toColor[i].first;
          unsigned fixReg = toColor[i].second;

          //put value back
          for (MCRegUnitIterator Units(origPR, TRI); Units.isValid(); ++Units) {
            ru2reg[*Units] = fixReg;
          }
  
          //change back to orig mapping
          unsigned newPR = virt2info[fixReg]->phys;
          virt2info[fixReg]->SetPhys(origPR);

          if (newPR){
            //erase the temporary mapping
            for (MCRegUnitIterator Units(newPR, TRI); Units.isValid(); ++Units) {
              ru2reg.erase(*Units);
            }
          }
        }
      }//next reg
    }//virtual register

    //can't find a reg, let's recolor
    loMap[mbb].insert(reg);
    return Recolor(loMap[mbb], mbb);
  }//end of try assign

  //simply color based on priority
  //  on already sorted live values
  const TargetRegisterClass* RegisterPressureReleaser::TryColor(MachineBasicBlock* curBB){
    CurLiveRegs& live = loMap[curBB];
    return Recolor(live, curBB);
  }

  MachineInstr* RegisterPressureReleaser::GetTerminator(MachineBasicBlock* predBB, MachineBasicBlock* succBB){
    //CODE COPIED FROM AsmPrinter.cpp
    // Go through the terminators to find the one that connects predBB->succBB
    for (auto &MI : predBB->terminators()) {
      // If it is not a simple branch, we are in a table somewhere.
      if (!MI.isBranch() || MI.isIndirectBranch())
        continue;
    
      // If we are the operands of one of the branches, this is not a fall
      // through. Note that targets with delay slots will usually bundle
      // terminators with the delay slot instruction.
      for (ConstMIBundleOperands OP(&MI); OP.isValid(); ++OP) {
        // the jumpto operand is succBB
        if (OP->isMBB() && OP->getMBB() == succBB)
          return &MI;
      }
    }

    //fall through: couldn't find a branching/jumping instruction that points to succBB
    return NULL;
  }

  /*actually an extended local register allocator*/
  // keep records of spill decision making and restores
  void RegisterPressureReleaser::ReleaseRegisterPressure(){
    MachineBasicBlock *Entry = MF->begin();
    ReversePostOrderTraversal<MachineBasicBlock*> RPOT(Entry);
    for (ReversePostOrderTraversal<MachineBasicBlock*>::rpo_iterator
           MBBI = RPOT.begin(), MBBE = RPOT.end(); MBBI != MBBE; ++MBBI) {

      MachineBasicBlock* curBB = *MBBI;
      PrepBBBegin(curBB);

      //MachineBasicBlock* curBB = *I;
      CurLiveRegs& curlo = loMap[curBB];
      VIRT2INFO & virt2info = virt2infos[curBB];
      std::map<unsigned, unsigned>& ru2reg = ru2regs[curBB];
      RU2LOC& localPCP = pressureChangePoints[curBB];

      /*----now traverse within the block----*/
      for (MachineBasicBlock::iterator InstIter = curBB->begin(), IE = curBB->end();
             InstIter != IE; ++InstIter) {
        MachineInstr * MI = InstIter;

        const MCInstrDesc &MCID = TM->getInstrInfo()->get(MI->getOpcode());
        /*----check uses----*/
        
        //marking the killed uses
        std::vector<unsigned> killedPhys;
#ifndef RPR_DEBUG
        CurLiveRegs killedVirts(SimpleRegCompare(MF));
#else
        CurLiveRegs killedVirts(SimpleRegCompare(MF, &regOrders));
#endif
        //marking live virt regs to update
        std::map<unsigned, unsigned> updateVirts;//with dists

        /*implicit uses: an instruction can implicitly use physical registers
         * check live and killed but nothing else
         */
        for (const uint16_t *ImplicitUses = MCID.ImplicitUses;
             ImplicitUses != NULL && *ImplicitUses; ++ImplicitUses) {
          unsigned physReg = *ImplicitUses;
          if (physReg != 0 && !MRI->isReserved(physReg)){
            assert(livePhysRegs.find(physReg) != livePhysRegs.end() && "physReg not in a register");
            //physregs are only live within block, this is enough
            unsigned dist = NAA->GetDistance(physReg, MI, curBB);
            if (dist == INFINITY_DIST)
              killedPhys.push_back(physReg);
          }
        }//end implicit uses

        /*remove virtregs used in this insturction from regdists(can spill list)*/
        for (unsigned i = 0, e = MI->getNumOperands(); i != e; ++i) {
          MachineOperand* Oper = &MI->getOperand(i);
          if (Oper->isReg() && Oper->getReg() != 0 && Oper->isUse()){
            unsigned reg = Oper->getReg();
            if (TargetRegisterInfo::isVirtualRegister(reg)){
              VIRT2INFO::iterator vit = virt2info.find(reg);
              if (vit != virt2info.end()){
                regDists[curBB].erase(vit->second);
              }
              //otherwise not live and not in regdists
            }
          }
        }

        /*explicit uses: uses you see in an instruction*/
        for (unsigned i = 0, e = MI->getNumOperands(); i != e; ++i) {
          MachineOperand* Oper = &MI->getOperand(i);
          if (Oper->isReg() && Oper->getReg() != 0 && Oper->isUse()){
            unsigned reg = Oper->getReg();
            
            //an allocatable phys reg
            //check live and killed but nothing else
            if (TargetRegisterInfo::isPhysicalRegister(reg)){
              //don't do anything with reserved registers
              if(MRI->isReserved(reg))
                continue;
              assert(livePhysRegs.find(reg) != livePhysRegs.end() && "physReg not in a register");
              
              //physregs are only live within block, this is enough
              unsigned dist = NAA->GetDistance(reg, MI, curBB);
              if (dist == INFINITY_DIST)
                killedPhys.push_back(reg);
            }
            
            else{//virtual reg
              CurLiveRegs::iterator found = curlo.find(reg);
            
              /*if not in a physreg, restore*/
              if (found == curlo.end()){
                assert(TargetRegisterInfo::isVirtualRegister(reg) && "physical registers are not spilled");
                //restore at MI, spill one if needed
                // restore handle live sets and distances
                Restore(reg, MI);
              }
            
              MachineBasicBlock* spillBB = FindSpillBlock(reg, curBB);
            
              unsigned dist;
              //nextAccess only return something if in the same BB
              //NextDist shows if the value is dead
              // In addition, distances are calculated with the consideration that
              //   a spill dominates it for they serve purpose only for spilling
              if (spillBB)
                  dist = NAA->GetDistance(reg, MI, spillBB);
              else
                  dist = NAA->GetDistance(reg, MI, curBB);
            
              if (dist == INFINITY_DIST)
                killedVirts.insert(reg);
              else
                updateVirts[reg] = dist;
            }//phys reg or virt reg
          }//a use reg
        }//explicit uses

        /*dead uses are not in curlo anymore*/
        CurLiveRegs::iterator it;
        for(it = killedVirts.begin(); it != killedVirts.end(); ++it){
          unsigned reg = *it;
          curlo.erase(reg);
          RegInfo* info = virt2info[reg];
          unsigned physReg = info->phys;
          for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
            unsigned ru = *Units;
            ru2reg.erase(ru);
            localPCP[ru] = MI;
          }
        }

        for (unsigned i = 0; i < killedPhys.size(); i++){
          unsigned physReg = killedPhys[i];
          livePhysRegs.erase(physReg);
          for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
            unsigned ru = *Units;
            ru2reg.erase(ru);
            localPCP[ru] = MI;
          }
        }

        /*update distances*/
        for (std::map<unsigned, unsigned>::iterator upit = updateVirts.begin();
                                                    upit != updateVirts.end(); ++upit){
          unsigned reg = upit->first;
          unsigned dist = upit->second;
          if (virt2info.find(reg) == virt2info.end()){
            virt2info[reg] = new RegInfo(reg, 0, dist);
          }
          RegInfo* info = virt2infos[curBB][reg];

          //need to get rid of the old one first
          //should have been erased
          regDists[curBB].erase(info);

          //add/sort with and the new dist info
          info->SetDist(dist);
          regDists[curBB].insert(info);
        }


        //for dead defs
        killedPhys.clear();
        killedVirts.clear();
        updateVirts.clear();

        /*implicit defs
         * all physical registers
         *   put them in registers, mark as live
         */
        for (const uint16_t *ImplicitDefs = MCID.ImplicitDefs;
             ImplicitDefs != NULL && *ImplicitDefs; ++ImplicitDefs) {
          unsigned reg = *ImplicitDefs;
          //mightUsedRU update at defs
          for (MCRegUnitIterator Units(reg, TRI); Units.isValid(); ++Units) {
            mightUsedRU.insert(*Units);
          }

          if (reg != 0 && !MRI->isReserved(reg)) {
            //mark as live
            livePhysRegs.insert(reg);
  
            //probably not happening
            // a dead def still needs to be killed after the instruction
            unsigned dist = NAA->GetDistance(reg, MI, curBB);
            if (dist == INFINITY_DIST)
              killedPhys.push_back(reg);
            //put into the physreg
            //ignore dead defs
            if(!TryAssignPhysReg(reg, curBB)){
              const TargetRegisterClass* RC = Recolor(curlo, curBB);
              if (MI == curBB->begin())
                recolorLoc[curBB] = NULL;
              else
                recolorLoc[curBB] = MI;//&*std::next(MachineBasicBlock::reverse_iterator(MI));

              if (!RC){
                Spill(MI, RC, curBB);
                Recolor(curlo, curBB);
              }
            }
          }
        }//end implicit defs

        /*explicit defs
         */
        for (unsigned i = 0, e = MI->getNumOperands(); i != e; ++i) {
          MachineOperand* Oper = &MI->getOperand(i);
          if (Oper->isReg() && (Oper->getReg() != 0) && Oper->isDef()) {
            unsigned reg = Oper->getReg();
            if (TargetRegisterInfo::isPhysicalRegister(reg)){
              //don't do anything with reserved registers
              if (MRI->isReserved(reg))
                continue;
              //mark as live
              livePhysRegs.insert(reg);

              //probably not happening
              // a dead def still needs to be killed after the instruction
              unsigned dist = NAA->GetDistance(reg, MI, curBB);
              if (dist == INFINITY_DIST)
                killedPhys.push_back(reg);
              //put into the physreg
              //ignore dead defs
              else if(!TryAssignPhysReg(reg, curBB)){
                const TargetRegisterClass* RC = Recolor(curlo, curBB);
                if (MI == curBB->begin())
                  recolorLoc[curBB] = NULL;
                else
                  recolorLoc[curBB] = MI;//&*std::next(MachineBasicBlock::reverse_iterator(MI));

                if (!RC){
                  Spill(MI, RC, curBB);
                  Recolor(curlo, curBB);
                }
              }
            }

            else{//virtual registers
              CurLiveRegs::iterator it = curlo.find(reg);
              //not live
              if (it == curlo.end()){
                curlo.insert(reg);
                //if it's not in live, it's probably a reg not yet seen in this block
                if (virt2info.find(reg) == virt2info.end())
                  virt2info[reg] = new RegInfo(reg, 0, 0);
                  
                const TargetRegisterClass* highPressureRC = TryAssign(reg, curBB);
                if (highPressureRC){
                  MachineInstr* spillLoc = Spill(MI, highPressureRC, curBB);
                  if (MI == curBB->begin())
                    recolorLoc[curBB] = NULL;
                  else if (spillLoc != NULL)
                    recolorLoc[curBB] = spillLoc;//&*std::next(MachineBasicBlock::reverse_iterator(MI));

                  //mightUsedRU updated in recolor
                  Recolor(curlo, curBB);
                }
                else{
                  unsigned assignedPhsReg = virt2info[reg]->phys;
                  //mightUsedRU update at defs
                  for (MCRegUnitIterator Units(assignedPhsReg, TRI); Units.isValid(); ++Units) {
                    mightUsedRU.insert(*Units);
                  }
                }
              }
                
              unsigned dist;
              MachineBasicBlock* spillbb = FindSpillBlock(reg, curBB);

              //never spilled or spill doesn't dominates this MI
              if (spillbb)
                  dist = NAA->GetDistance(reg, MI, spillbb);
              else//should have been spilled
                  dist = NAA->GetDistance(reg, MI, MI->getParent());
            
              if (dist != INFINITY_DIST){
                updateVirts[reg]= dist;
                curlo.insert(reg);
              }
              else{
                //dead code should have been eliminated
                killedVirts.insert(reg);
              }
            }

          }//if reg
        }//for operator

        //this really would happen
        for (unsigned i = 0; i < killedPhys.size(); i++){
          unsigned physReg = killedPhys[i];
          livePhysRegs.erase(physReg);
          for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
            ru2reg.erase(*Units);
            localPCP[*Units] = MI;
          }
        }

        for (it = killedVirts.begin(); it != killedVirts.end(); ++it){
          unsigned reg = *it;
          curlo.erase(reg);
          RegInfo* info = virt2info[reg];
          unsigned physReg = info->phys;
          for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
            ru2reg.erase(*Units);
            localPCP[*Units] = MI;
          }
          info->ClearPhys();
        }

        /*update distances*/
        for (std::map<unsigned, unsigned>::iterator upit = updateVirts.begin();
                                                    upit != updateVirts.end(); ++upit){
          unsigned reg = upit->first;
          unsigned dist = upit->second;
          if (virt2info.find(reg) == virt2info.end()){
            virt2info[reg] = new RegInfo(reg, 0, dist);
          }
          RegInfo* info = virt2infos[curBB][reg];

          //need to get rid of the old one first
          //should have been erased
          regDists[curBB].erase(info);

          //add/sort with and the new dist info
          info->SetDist(dist);
          regDists[curBB].insert(info);
        }

        if (spills.find(curBB) == spills.end())
          continue;

        for (unsigned i = 0, e = MI->getNumOperands(); i != e; ++i) {
          MachineOperand* Oper = &MI->getOperand(i);
          if (Oper->isReg() && (Oper->getReg() != 0) && Oper->isDef()) {
            unsigned reg = Oper->getReg();
            if (spills[curBB].find(reg) == spills[curBB].end())
              continue;

            if (*spills[curBB][reg].rbegin() == MI){
              curlo.erase(reg);
              RegInfo* info = virt2infos[curBB][reg];
              unsigned physReg = info->phys;
              for (MCRegUnitIterator Units(physReg, TRI); Units.isValid(); ++Units) {
                ru2reg.erase(*Units);
                localPCP[*Units] = MI;
              }
              info->ClearPhys();
            }
          }
        }

      }//for instruction
    }

    SlotMap::iterator si;
    for (si = slots.begin(); si != slots.end(); ++si){
      unsigned spillReg = si->first;
      spilledRegs.insert(spillReg);
    }
  }

  /*Try to find the following:
   *  1. max register class weight
   *  2. register order for each reg class based on the popularity of registers
   *others to add...
   */
  void RegisterPressureReleaser::GetMoreRCI(){
    RCI.runOnMachineFunction(*MF);
    //max register class weight
    TargetRegisterInfo::regclass_iterator classit;
    //The more classes that include a register, the more popular it is.

    for (classit = TRI->regclass_begin(); classit != TRI->regclass_end(); ++classit){
      const TargetRegisterClass *rc = *classit;
      ArrayRef<MCPhysReg> order = RCI.getOrder(rc);
      regOrders[rc] = order;
    }
  }

  static void PrintLoopinfo(MachineLoopInfo* LoopInfo) {
/*    for (MachineLoop::iterator iter = LoopInfo->begin(),
         iterEnd = LoopInfo->end(); iter != iterEnd; ++iter) {
      (*iter)->print(dbgs(), 0);
    }*/

    std::vector<MachineLoop *> NestedLoops;
    for (MachineLoopInfo::iterator It = LoopInfo->begin(), E = LoopInfo->end(); It != E; ++It)
      for (MachineLoop *ML : depth_first(*It))
        NestedLoops.push_back(ML);

//    for (unsigned i = 0; i < NestedLoops.size(); i++){ 
//      NestedLoops[i]->print(dbgs(), 0);
//    }
  }

  bool RegisterPressureReleaser::runOnMachineFunction(MachineFunction& F){
    /*initialization*/
    //info
    MF = &F;
    MRI = &MF->getRegInfo();
    TM = &MF->getTarget();
    TRI = TM->getRegisterInfo();
    TII = TM->getInstrInfo();

    if (NORPR)
      return true;

    GetMoreRCI();
    
    //analysis
    DT  = &getAnalysis<MachineDominatorTree>();
    PDT = &getAnalysis<MachinePostDominatorTree>();
    LIS = &getAnalysis<LiveIntervals>();
    LSS = &getAnalysis<LiveStacks>();
    NAA = &getAnalysis<NextAccessAnalyzer>();
    VRM = &getAnalysis<VirtRegMap>();
    SLOTINDEXES = &getAnalysis<SlotIndexes>();
    Loops = &getAnalysis<MachineLoopInfo>();

    PrintLoopinfo(Loops);


    /*run*/
    //run on machine function
    //  belady's min
    //  push restores up
    ReleaseRegisterPressure();
    //TODO: 1. this first or add spill first? probably this first for additional restore info
    //      2. add restores and such at bb edge? or at the bb begin with potential extra cost
    AddRemaningSpillsAndRestores();//spill(store instruction) placement
    DealWithBBEdges();
    AddLoadAndStores();
    AddLiveStackIntervals();//with added spills and restores
    UpdateSpilledLiveRanges();

    //update program info according to changes
    //cfg might be changed in deal with bb
//    if(cfgChanged){
      DT->releaseMemory();
      DT->runOnMachineFunction(F);
      PDT->releaseMemory();
      PDT->runOnMachineFunction(F);
//    }

    return true;
  }

/*=================DEBUGGIN RELATED===================*/
  void RegisterPressureReleaser::PrintAllocatableRegs(){
    unsigned numRegs = MRI->getTargetRegisterInfo()->getNumRegs();
    // Iterate over reg uses/defs to collect unused PR
    dbgs() << "Allocatable Regs:\n";
    for (unsigned physReg = 1; physReg != numRegs; physReg++){
      if (MRI->isReserved(physReg))
        continue;
      dbgs() << TRI->getName(physReg) << " ";
    }
    dbgs() << "\n\n";
  }

  unsigned RegisterPressureReleaser::GetReg(CurLiveRegs& virts, unsigned index){
    if (index >= virts.size())
      return 0;
    CurLiveRegs::iterator it;
    unsigned i;
    for (it = virts.begin(); it!= virts.end(); ++it, i++){
      if (i == index)
        return *it;
    }
    return 0;
  }

  void RegisterPressureReleaser::PrintOrder(const TargetRegisterClass* rc){
    ArrayRef<MCPhysReg> order = regOrders[rc];
    dbgs() << rc->getName() << ":\n";
    for (unsigned i = 0; i < order.size(); i++){
      dbgs() << PrintReg(order[i], TRI) << " ";
    }
    dbgs() << "\n\n";
  }

  void RegisterPressureReleaser::PrintVirtSet(CurLiveRegs& virts){
    CurLiveRegs::iterator rit;
    dbgs() << "live phys regs:\n";
    for (rit = virts.begin(); rit != virts.end(); ++rit){
      dbgs() << PrintReg(*rit, TRI) << " ";
    }
    dbgs() << "\n\n";
  }

  void RegisterPressureReleaser::PrintLiveVirts(MachineBasicBlock* mbb){
    CurLiveMap::iterator it = loMap.find(mbb);
    if (it == loMap.end()){
      dbgs() << "no live virtregs\n";
    } 
    else{
      PrintVirtSet(it->second);
    }
  }

  void RegisterPressureReleaser::PrintLivePhys(){
    CurLivePhysRegs::iterator it;
    dbgs() << "no live physregs\n";
    for (it = livePhysRegs.begin(); it != livePhysRegs.end(); ++it){
      dbgs() << PrintReg(*it, TRI) << " ";
    }
    dbgs() << "\n\n";
  }

  void RegisterPressureReleaser::PrintRegInfo(MachineBasicBlock* mbb){
    VIRT2INFO& virt2info = virt2infos[mbb];
    VIRT2INFO::iterator it;
    for (it = virt2info.begin(); it != virt2info.end(); ++it){
      RegInfo* info = it->second;
      dbgs() << "(" << PrintReg(info->virt) << ", " << info->phys << ", " << info->dist << ") ";
    }
    dbgs() << "\n";
  }

  void RegisterPressureReleaser::PrintRegDists(MachineBasicBlock* mbb){
    RegDists& dists = regDists[mbb];
    RegDists::iterator it;
    for (it = dists.begin(); it != dists.end(); ++it){
      RegInfo* dist = *it;
      dbgs() << "(" << PrintReg(dist->virt) << ", " << dist->dist << ") ";
    }
    dbgs() << "\n";
  }

  void RegisterPressureReleaser::PrintLiveIntervals(){
    for (unsigned i = 0; i < MRI->getNumVirtRegs(); i++){
      unsigned virt = TRI->index2VirtReg(i);
      if (LIS->hasInterval(virt))
        LIS->getInterval(virt).dump();
    }
  }

  void RegisterPressureReleaser::PrintSpillLocations(){
    SlotMap::iterator si;
    std::set<unsigned> spilledRegs;
    for (si = slots.begin(); si != slots.end(); ++si){
      unsigned spillReg = si->first;
      dbgs() << PrintReg(spillReg, TRI) << ":\n";
      SpillMap::iterator it;
      for (it = spills.begin(); it != spills.end(); ++it){
        SpillInstrs::iterator rit = it->second.find(spillReg);
        if (rit == it->second.end())
          continue;

        //TODO: fix this
        //std::vector<MachineInstr*>& locations = rit->second;
        //for (unsigned i = 0; i < locations.size(); i++)
        //  LIS->getInstructionIndex(locations[i]).dump();
      }
    }
  }

  void RegisterPressureReleaser::PrintRestoreLocations(){
      SlotMap::iterator si;
      std::set<unsigned> spilledRegs;
      for (si = slots.begin(); si != slots.end(); ++si){
        unsigned spillReg = si->first;
        dbgs() << PrintReg(spillReg, TRI) << ":\n";
        SpillMap::iterator it;
        for (it = restores.begin(); it != restores.end(); ++it){
          SpillInstrs::iterator rit = it->second.find(spillReg);
          if (rit == it->second.end())
            continue;

        //TODO: fix this      
        //  std::vector<MachineInstr*>& locations = rit->second;
        //  for (unsigned i = 0; i < locations.size(); i++)
        //    LIS->getInstructionIndex(locations[i]).dump();
        }
      }

  }

