//===- NextAccessAnalysis.h - Function to record next access of regs and distances to access ----*- C++ -*-===//
///
/// Next Access contains:
///     1. All access within a given block and
///     2. First uses of blocks reachable from a given block
///
/// Next Use Distances:
///   Within a block:
///     1. Next access is a def (kill) : infinity
///     2. Next access is a use        : distance to the use
///     3. No next accesses            :
///        1) dead: infinity
///        2) else: distance to block end + distance between
///                 block end and next use out side of the block
///
///   NOTE: holes on some paths but not all
///   For instance, sa reg R is live in a block B which has two branches X and Y.
///   Suppose branch X has a hole from block begin to a def but branch Y has only uses
///   R is in liveout of B, livein of Y but not in livein of X.
///     1. If R is to be spilled in B, the spilled value is only valid up to
///        def of R in X and valid in Y
///        So the distance for R to be spilled in B is distance merge of def in X and use in Y.
///     2. If R is to be spilled in X before def, then it's dead from the beginning,
///        which can be considered spilled
///     3. If R is to be spilled in Y, it's the normal case
///
//===----------------------------------------------------------------------===//

#include "NextAccessAnalysis.h"
#include "llvm/ADT/PostOrderIterator.h"
#include "llvm/IR/CFG.h"
#include "llvm/CodeGen/MachineBranchProbabilityInfo.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineLoopInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/LiveInterval.h"
#include "llvm/CodeGen/Passes.h"


#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetOpcodes.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"

#include <limits>

using namespace llvm;
  #define DEBUG_TYPE "regalloc"
//=======================Set up and stuff============================

  char NextAccessAnalyzer::ID = 0;
  char &llvm::NextAccessAnalyzerID = NextAccessAnalyzer::ID;
  INITIALIZE_PASS_BEGIN(NextAccessAnalyzer, "nextaccessanalyzer",
                    "Next Access Analysis", false, false)
  INITIALIZE_PASS_DEPENDENCY(MachineBranchProbabilityInfo)
  INITIALIZE_PASS_DEPENDENCY(MachineDominatorTree)
  INITIALIZE_PASS_DEPENDENCY(LiveIntervals)
  INITIALIZE_PASS_END(NextAccessAnalyzer, "nextaccessanalyzer",
                    "Next Access Analysis", false, false)


  void NextAccessAnalyzer::getAnalysisUsage(AnalysisUsage &AU) const {
    AU.setPreservesCFG();
    //for merging distances
    AU.addRequired<MachineBranchProbabilityInfo>();
    AU.addRequired<MachineDominatorTree>();
    AU.addPreserved<MachineDominatorTree>();
    AU.addRequired<MachinePostDominatorTree>();
    AU.addPreserved<MachinePostDominatorTree>();
    AU.addPreserved<SlotIndexes>();
    AU.addRequired<LiveIntervals>();
    AU.addPreserved<LiveIntervals>();
    MachineFunctionPass::getAnalysisUsage(AU);
  }
  
  NextAccessAnalyzer::NextAccessAnalyzer() : MachineFunctionPass(ID){
    firstTimeRunning = true;
    initializeLiveIntervalsPass(*PassRegistry::getPassRegistry());
  }

  NextAccessAnalyzer::~NextAccessAnalyzer(){
  }

  void NextAccessAnalyzer::releaseMemory(){
    //free the regaccess data
    std::vector<RegAccess*>::iterator it;
    for (it = accessVector.begin(); it != accessVector.end(); ++it){
      delete *it;
    }
    accessVector.clear();

    inBBAccs.clear();
    notInBBAccs.clear();
    distToBBBegin.clear();
    distToBBEnd.clear();
  }

//=======================end of Set up and debug now============================
//  void NextAccessAnalyzer::print(raw_ostream & O,const Module * M){
//  }

//=======================end of debug============================

  MachineInstr* NextAccessAnalyzer::GetLastDef(unsigned reg, MachineBasicBlock* mbb){
    if (inBBAccs.find(mbb) == inBBAccs.end())
      return NULL;

    InRegAccsMap& regAccs = inBBAccs[mbb];
    if (regAccs.find(reg) == regAccs.end())
      return NULL;

    RegAccessesWithOrder accs = regAccs[reg];
    RegAccessesWithOrder::reverse_iterator it = accs.rbegin();
    RegAccessesWithOrder::reverse_iterator accsBegin = accs.rend();

    for (; it != accsBegin; ++it){
      RegAccess* ac = *it;
      if (ac->isUse)
        continue;
      return ac->accInst;
    }

    //all uses
    return NULL;
  }

  MachineInstr* NextAccessAnalyzer::GetFirstDef(unsigned reg, MachineBasicBlock* mbb){
    if (inBBAccs.find(mbb) == inBBAccs.end())
      return NULL;

    InRegAccsMap& regAccs = inBBAccs[mbb];
    if (regAccs.find(reg) == regAccs.end())
      return NULL;

    RegAccessesWithOrder accs = regAccs[reg];
    RegAccessesWithOrder::iterator it = accs.begin();
    RegAccessesWithOrder::iterator accsEnd = accs.end();

    for (; it != accsEnd; ++it){
      RegAccess* ac = *it;
      if (ac->isUse)
        continue;
      return ac->accInst;
    }

    //all uses
    return NULL;
  }

  //is spill == is use
  void NextAccessAnalyzer::InsertAccess(unsigned reg, MachineInstr* acc, bool isSpill){
    MachineBasicBlock* mbb = acc->getParent();
    RegAccess* newAcc = new RegAccess(acc, LIS->getInstructionIndex(acc), isSpill);
    inBBAccs[mbb][reg].insert(newAcc);
  }

  MachineInstr* NextAccessAnalyzer::GetNextDef(unsigned reg, MachineInstr* def){
    MachineBasicBlock* mbb = def->getParent();
    if (inBBAccs.find(mbb) == inBBAccs.end())
      return NULL;

    InRegAccsMap& regAccs = inBBAccs[mbb];
    if (regAccs.find(reg) == regAccs.end())
      return NULL;

    RegAccessesWithOrder accs = regAccs[reg];
    RegAccessesWithOrder::iterator it = accs.begin();
    RegAccessesWithOrder::iterator accsBegin = accs.end();
    RegAccess* ac;

    //locate def in accs of curBB
    for (; it != accsBegin; ++it){
      ac = *it;
      if (ac->accInst == def && ac->isUse == false){
        ++it;//so it's the next access
        break;
      }
    }

    for (; it != accsBegin; ++it){
      ac = *it;
      if (!ac->isUse)
        return ac->accInst;
    }

    //all uses before the input def
    return NULL;
  }


  MachineInstr* NextAccessAnalyzer::GetPrevDef(unsigned reg, MachineInstr* def){
    MachineBasicBlock* mbb = def->getParent();
    if (inBBAccs.find(mbb) == inBBAccs.end())
      return NULL;

    InRegAccsMap& regAccs = inBBAccs[mbb];
    if (regAccs.find(reg) == regAccs.end())
      return NULL;

    RegAccessesWithOrder accs = regAccs[reg];
    RegAccessesWithOrder::reverse_iterator it = accs.rbegin();
    RegAccessesWithOrder::reverse_iterator accsBegin = accs.rend();
    RegAccess* ac;


    //locate def in accs of curBB
    for (; it != accsBegin; ++it){
      ac = *it;
      if (ac->accInst == def){
        ++it;//so it's the next access
        break;
      }
    }

    for (; it != accsBegin; ++it){
      ac = *it;
      if (!ac->isUse)
        return ac->accInst;
    }

    //all uses before the input def
    return NULL;
  }

  RegAccess* NextAccessAnalyzer::GetLastAccess(unsigned reg, MachineBasicBlock* mbb){
    if (inBBAccs.find(mbb) == inBBAccs.end())
      return NULL;

    if (inBBAccs[mbb].find(reg) == inBBAccs[mbb].end())
      return NULL;

    return *(inBBAccs[mbb][reg].rbegin());
  }


  RegAccess* NextAccessAnalyzer::GetPrevAccess(unsigned reg, RegAccess* lastAcc){
    MachineInstr* mi = lastAcc->accInst;
    MachineBasicBlock* mbb = mi->getParent();

    RegAccessesWithOrder& accs = inBBAccs[mbb][reg];
    for (RegAccessesWithOrder::reverse_iterator it = accs.rbegin(); it != accs.rend(); ++it){
      RegAccess* ac = *it;
      if (ac == lastAcc){
        ++it;
        ac = *it;
        if (ac == *accs.rend())
          return NULL;
        return ac;
      }
    }

    //not reachable
    return NULL;
  }

  //TODO check if def-use in the same mi matters
  RegAccess* NextAccessAnalyzer::GetNextAccess(unsigned reg, MachineInstr* mi){
    MachineBasicBlock* mbb = mi->getParent();
    SlotIndex si = LIS->getInstructionIndex(mi);

    /*no access within mbb*/
    InRegAccsMap::iterator it = inBBAccs[mbb].find(reg);
    if (it == inBBAccs[mbb].end())
        return NULL;

    /*go through accesses with mbb*/    
    RegAccessesWithOrder& accs = it->second;
    RegAccessesWithOrder::iterator rit;
    for (rit = accs.begin(); rit != accs.end(); ++rit){
      RegAccess* acc = *rit;
      MachineInstr* accInst = acc->accInst;
      //within block
      SlotIndex asi = LIS->getInstructionIndex(accInst);
      if (asi <= si)//not the next access
        continue;
      else
        return acc;//next access within mbb
    }
    /*after the last access of reg in mbb*/
    return NULL; //NULL for next access outside of mbb
  }

  RegAccess* NextAccessAnalyzer::GetFirstAccess(unsigned reg, MachineBasicBlock* mbb){
    /*no access within mbb*/
    InRegAccsMap::iterator it = inBBAccs[mbb].find(reg);
    if (it == inBBAccs[mbb].end())
        return NULL;

    return *(it->second.begin());
  }

  /*the last block a given block dominates*/
  MachineBasicBlock* NextAccessAnalyzer::GetLastDomBlock(MachineBasicBlock* mbb){
    MachineBasicBlock* curBB = mbb;
    MachineDomTreeNode* idom = PDT->getNode(mbb)->getIDom();
    while(idom){
      MachineBasicBlock* idomBB = idom->getBlock();
      //curBB doesn't dominates the imediate post dom(merging point), return curBB
      if (!DT->dominates(curBB, idomBB))
        return curBB;
      curBB = idomBB;
      idom = idom->getIDom();
    }
    //no more idom
    //idom in the last iterator was the last one
    return curBB;
  }

  /*distance to first access in mbb*/
  unsigned NextAccessAnalyzer::FirstDist(unsigned reg, MachineBasicBlock* mbb){
    RegAccessesWithOrder& accs = inBBAccs[mbb][reg];
    SlotIndex asi = LIS->getInstructionIndex((*accs.begin())->accInst);
    SlotIndex bbsi = LIS->getMBBStartIdx(mbb);
    return bbsi.distance(asi);//dist within mbb
  }

  //TODO: add u1-d-u2 into consideration
  // should return dist(u1, u2)/2 instead of u1-d
  /* return distance of next access to block begin
     possible returns
              0. infinity if dead after this instruction
              1. distance within the basic block if the next use is with the basic block
              2. distance to the block end + distance of next use to the block end
              3. distance to the block end + distance from block end to post dominator's end
    */
  unsigned NextAccessAnalyzer::GetDistance(unsigned reg, MachineInstr* mi, MachineBasicBlock* spillMbb){
    MachineBasicBlock* mbb = mi->getParent();
    SlotIndex bbsi = LIS->getMBBStartIdx(mbb);
    //na for next access
    RegAccess* na = GetNextAccess(reg, mi);

    if (na != NULL){//within mbb
        if (!na->isUse)
          return INFINITY_DIST;
        SlotIndex asi = LIS->getInstructionIndex(na->accInst);
        return bbsi.distance(asi);//dist within mbb
    }
    else if (TargetRegisterInfo::isPhysicalRegister(reg)){
        return INFINITY_DIST;
    }
    else if (notInBBAccs[mbb].find(reg) == notInBBAccs[mbb].end()){
        return INFINITY_DIST;
    }
    else{
      //for check if a use instruction kills a value
      //if (LIS->getInterval(reg).Query(LIS->getInstructionIndex(mi)).isKill())
      //  return INFINITY_DIST;

      /*check dominance of spill location to next access in other blocks*/
      bool domAll = true;
      bool notAllDef = false;
      RegAccessesWithOutOrder& accs = notInBBAccs[mbb][reg];
      RegAccessesWithOutOrder::iterator it;
      RegAccessesWithOutOrder::iterator IE = accs.end();
      for (it = accs.begin(); it != IE; ++it){
        RegAccess* acc = *it;
        MachineBasicBlock* naMbb = acc->accInst->getParent();
        if (!DT->dominates(spillMbb, naMbb)){
          domAll = false;
        }

        notAllDef = notAllDef || acc->isUse;
      }

      //if all def
      if (!notAllDef)
        return INFINITY_DIST;

      //at least one use so not dead
      /*if spill location dominates all, use distance from analysis*/
      int blockLength = LIS->getMBBStartIdx(mbb).distance(LIS->getMBBEndIdx(mbb));
      if (domAll){
        return blockLength + distToBBEnd[mbb][reg];
      }
      //TODO: immediate post dominator
      else{//if not use distance to it's immediate post dominator
        return blockLength + distToBBEnd[mbb][reg] -
                             distToBBEnd[GetLastDomBlock(mbb)][reg];
      }
    }
  }

  /* dist to bb end of mbb merged using distance to bb begin of succ bbs
   * use block edge probability as weight
   * return true if dists changes
   *       false if dists don't change
   */
  bool NextAccessAnalyzer::MergeDistancesChange(MachineBasicBlock* mbb){
    std::map<unsigned, unsigned> regDists;
    std::map<unsigned, uint32_t> regWeights;
    for (MachineBasicBlock::const_succ_iterator SI  = mbb->succ_begin(),
                                          E   = mbb->succ_end();
                                          SI != E; ++SI) {
      uint32_t succWeight = MBPI->getEdgeWeight(mbb, SI);
      MachineBasicBlock* succBB = *SI;
      RegDistMap::iterator it;
      RegDistMap& dists = distToBBBegin[succBB];
      for (it = dists.begin(); it != dists.end(); ++ it){
        unsigned reg = it->first;
        unsigned dist = it->second;
        if(regDists.find(reg) == regDists.end()){
          regDists[reg] = 0;
        }
        regDists[reg] += dist;

        if(regWeights.find(reg) == regWeights.end()){
          regWeights[reg] = 0;
        }
        regWeights[reg] += succWeight;
      }
    }

    bool changed = false;
    RegDistMap& dists = distToBBEnd[mbb];
    RegDistMap& distsIn = distToBBBegin[mbb];
    std::map<unsigned, unsigned>::iterator it;
    unsigned blockDist = LIS->getMBBStartIdx(mbb).distance(LIS->getMBBStartIdx(mbb));
    for (it = regDists.begin(); it != regDists.end(); ++it){
      unsigned reg = it->first;
      //ohhhh noooooo!
      unsigned newDist = it->second / regWeights[reg];

      //check and update for reg
      if (dists.find(reg) == dists.end()){//new reg
        changed = true;
        dists[reg] = newDist;
        if (distsIn.find(reg) == distsIn.end()){
          distsIn[reg] = newDist + blockDist;
        }
      }
      else if (dists[reg] != newDist){//new dist
        changed = true;
        if (distsIn[reg] == dists[reg]){
            distsIn[reg] = newDist;
        }
        dists[reg] = newDist;
      }
    }
    return changed;
  }

  void NextAccessAnalyzer::AddRegAccess(unsigned reg){
    // Iterate over reg uses/defs.
    for (MachineRegisterInfo::reg_instr_iterator
         regItr = MRI->reg_instr_begin(reg);
         regItr != MRI->reg_instr_end(); ++regItr) {
  
      // Grab the use/def instr.
      MachineInstr *mi = &*regItr;
      MachineBasicBlock *curBB = mi->getParent();
      for (unsigned i = 0; i != mi->getNumOperands(); ++i) {
        MachineOperand &op = mi->getOperand(i);
        if (!op.isReg() || op.getReg() != reg)
          continue;
  
        RegAccess* newAcc = new RegAccess(mi, LIS->getInstructionIndex(mi), op.isUse());
        RegAccessesWithOrder& accs = inBBAccs[curBB][reg];
  
        //in case of multiple uses
        //it's another use in the same instruction, no need to keep multiple uses
        bool inserted = accs.insert(newAcc).second;
        if (!inserted)
          delete newAcc;
        else{
          //for deleting later
          accessVector.push_back(newAcc);
        }
      }
      //initial distance info
      distToBBBegin[curBB][reg] = FirstDist(reg, curBB);
    }  
  }

  //add appearances to blocks regs appear in
  //assuming physregs have liveintervals only in bb's
  //         physreg values propogates with copy to virt regs
  void NextAccessAnalyzer::PreAnalyzeAccess(){
    for (unsigned i = 0, e = MRI->getNumVirtRegs(); i != e; ++i) {
      unsigned reg = TargetRegisterInfo::index2VirtReg(i);
      if (!LIS->hasInterval(reg))
        continue;
      AddRegAccess(reg);
    }
  }

  //total number of next appearances of all regs for a given block
  unsigned NextAccessAnalyzer::CheckCount(MachineBasicBlock* mbb){
    NotInRegAccsMap& apps = notInBBAccs[mbb];
    unsigned count = 0;
    NotInRegAccsMap::iterator regit;
    for (regit = apps.begin(); regit != apps.end(); ++regit)
      count += regit->second.size();
    return count;
  }

  /*analysis marks next use location and next use distance, with respect to the distance function*/
  void NextAccessAnalyzer::AnalyzeAccess(){
    std::vector<MachineBasicBlock*> workOrder;//order to based on
    std::queue<MachineBasicBlock*> workList;  //list with which to proceed
    std::set<MachineBasicBlock*> workSet;     //bbs need proceccing

    MachineBasicBlock* entry = MF->begin();
    for (po_iterator<MachineBasicBlock*> it = po_begin(entry),
         e = po_end(entry); it != e; ++it) {
      workOrder.push_back(*it);
      workSet.insert(*it);
    }

    //iteratively add next use info
    while(!workSet.empty()){
      std::vector<MachineBasicBlock*>::iterator it;
      for (it = workOrder.begin(); it != workOrder.end(); ++it){
        if (workSet.find(*it)!= workSet.end())
          workList.push(*it);
      }
      workSet.clear();

      //each iteration
      while(!workList.empty()){
        MachineBasicBlock* curBB = workList.front();
        workList.pop();

        //curBB was added by one of the succ bb. it sees the change now
        workSet.erase(curBB);

        //for checking if new accesses are added later        
        unsigned oldCount = CheckCount(curBB);
        
        for (MachineBasicBlock::succ_iterator SI  = curBB->succ_begin(),
                                              E   = curBB->succ_end();
                                              SI != E; ++SI) {
          MachineBasicBlock* succBB = *SI;
          NotInRegAccsMap& notInCurBBAccs = notInBBAccs[curBB];

          std::set<unsigned> inRegs;
          //first process the accesses within succBB
          InRegAccsMap::iterator it;
          for (it = inBBAccs[succBB].begin(); it != inBBAccs[succBB].end(); ++it){
            unsigned reg = it->first;
            inRegs.insert(reg);
            //add the first one from the within curbb accesses
            //it->second gives all within curbb access
            //.begin() gives the iterator to the first access
            notInCurBBAccs[reg].insert(*(it->second.begin()));
          }

          //now process accesses out side of succBB
          NotInRegAccsMap::iterator nit;
          for (nit = notInBBAccs[succBB].begin(); nit != notInBBAccs[succBB].end(); ++nit){
            unsigned reg = nit->first;
            //no access in succbb
            if (inRegs.find(reg) == inRegs.end()){
                //nit->second gives the set of accesses out side of succBB
                notInCurBBAccs[reg].insert(nit->second.begin(), nit->second.end());
            }
          }
        }

        //check if new accesses are added
        unsigned newCount = CheckCount(curBB);
        if (oldCount != newCount){
          //?should do this? workSet.insert(curBB);
          //add pred of curbb as they might change as well
          for (MachineBasicBlock::pred_iterator PI  = curBB->pred_begin(),
                                                E   = curBB->pred_end();
                                                PI != E; ++PI) {
            workSet.insert(*PI);
          }
        }//if new uses
      }//for bb
    }//if changed
  }//analyze access

  //
  void NextAccessAnalyzer::AnalyzeDists(){
    std::vector<MachineBasicBlock*> workOrder;//order to based on
    std::queue<MachineBasicBlock*> workList;  //list with which to proceed
    std::set<MachineBasicBlock*> workSet;     //bbs need proceccing
    
    MachineBasicBlock* entry = MF->begin();
    for (po_iterator<MachineBasicBlock*> it = po_begin(entry),
         e = po_end(entry); it != e; ++it) {
      workOrder.push_back(*it);
      workSet.insert(*it);
    }

    
    //iteratively add next use info
    while(!workSet.empty()){
      std::vector<MachineBasicBlock*>::iterator it;
      for (it = workOrder.begin(); it != workOrder.end(); ++it){
        if (workSet.find(*it)!= workSet.end())
          workList.push(*it);
      }
      workSet.clear();

      while(!workList.empty()){
        MachineBasicBlock* curBB = workList.front();
        workList.pop();
        
        //curBB was added by one of the succ bb. it sees the change now
        workSet.erase(curBB);

        NotInRegAccsMap::iterator rit;
        //add pred bbs because dist in curbb changed
        if (MergeDistancesChange(curBB)){
          //?should do this? workSet.insert(curBB);
          //add pred of curbb as they might change as well
          for (MachineBasicBlock::pred_iterator PI  = curBB->pred_begin(),
                                                E   = curBB->pred_end();
                                                PI != E; ++PI) {
            workSet.insert(*PI);
          }
        }//if merge dist changes
      }//for bbs
    }//if changes

    workOrder.clear();
  }//analyze dists

  void NextAccessAnalyzer::AnalyzePhysReg(){
    unsigned numRegs = MRI->getTargetRegisterInfo()->getNumRegs();
    // Iterate over reg uses/defs.
    unsigned physReg;
    for (physReg = 1; physReg != numRegs; physReg++){
      AddRegAccess(physReg);
    }
  }

  void NextAccessAnalyzer::Analyze(){
    PreAnalyzeAccess();
    AnalyzeAccess();
    AnalyzeDists();
    //don't propogate next access info for physregs
    //they are assumed to be only live within bbs
    AnalyzePhysReg();
  }

  bool NextAccessAnalyzer::runOnMachineFunction(MachineFunction &mf) {
    //GET ANALYSIS
    MF   = &mf;
    MRI  = &(MF->getRegInfo());
    LIS  = &getAnalysis<LiveIntervals>();
    DT   = &getAnalysis<MachineDominatorTree>();
    PDT  = &getAnalysis<MachinePostDominatorTree>();
    MBPI = &getAnalysis<MachineBranchProbabilityInfo>();

    //ANALYZE
    Analyze();
    return true;
  }

