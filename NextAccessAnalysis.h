#ifndef NEXTACCESSANALYSIS_H
#define NEXTACCESSANALYSIS_H
//===- NextAccessAnalysis.h - Function to record next access of regs and distances to access ----*- C++ -*-===//
///
/// Next Access contains:
///     1. All access within a given block and
///     2. First uses of blocks reachable from a given block
///
/// Next Use Distances:
///   Within a block:
///     1. Next access is a def (kill) : infinity
///     2. Next access is a use        : distance to the use
///     3. No next accesses            :
///        1) dead: infinity
///        2) else: distance to block end + distance between
///                 block end and next use out side of the block
///
///   NOTE: holes on some paths but not all
///   For instance, sa reg R is live in a block B which has two branches X and Y.
///   Suppose branch X has a hole from block begin to a def but branch Y has only uses
///   R is in liveout of B, livein of Y but not in livein of X.
///     1. If R is to be spilled in B, the spilled value is only valid up to
///        def of R in X and valid in Y
///        So the distance for R to be spilled in B is distance merge of def in X and use in Y.
///     2. If R is to be spilled in X before def, then it's dead from the beginning,
///        which can be considered spilled
///     3. If R is to be spilled in Y, it's the normal case
///
//===----------------------------------------------------------------------===//

#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/LiveInterval.h"
#include "llvm/CodeGen/LiveIntervalAnalysis.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachinePostDominators.h"
#include <queue> 
#include <set>
#include <list>
#include <map>
//#include "BuildLiveSets.h"

#define INFINITY_DIST std::numeric_limits<int>::max()

namespace llvm {
  /*next use related*/
  /**/
  typedef struct RegAccess{
    MachineInstr* accInst;      // so that we can locate it. check dominance and insert restores if needed
    bool isUse;
    SlotIndex loc;

    RegAccess(MachineInstr* nu, SlotIndex si, bool iu){
        accInst = nu;
        isUse = iu;
        loc = si;
    }

    RegAccess(struct RegAccess* acc){
        accInst = acc->accInst;
        isUse = acc->isUse;
        loc = acc->loc;
    }
  }RegAccess;
  
  struct compareOrder{
    bool operator() (const RegAccess* lhs, const RegAccess* rhs) const{
      if(lhs->accInst == rhs->accInst)
        return lhs->isUse && !rhs->isUse;
      return lhs->loc < rhs->loc;
    }
  };

  typedef std::set<RegAccess*, struct compareOrder> RegAccessesWithOrder;         //set of accesses
  typedef std::set<RegAccess*> RegAccessesWithOutOrder;                    //set of accesses

  //Next access inside a given block
  typedef std::map<unsigned, RegAccessesWithOrder> InRegAccsMap;
  typedef std::map<MachineBasicBlock*, InRegAccsMap> InBBAccsMap;

  //next access out side of a given bb
  typedef std::map<unsigned, RegAccessesWithOutOrder> NotInRegAccsMap;
  typedef std::map<MachineBasicBlock*, NotInRegAccsMap> NotInBBAccsMap;

  typedef std::map<unsigned, unsigned> RegDistMap; //dist to next access or the length of the following hole
  typedef std::map<MachineBasicBlock*, RegDistMap> BBDistMap;  //dist/lengths for regs

  /*analysis pass*/
  class NextAccessAnalyzer : public MachineFunctionPass{
/*========members========*/
    private:
      bool firstTimeRunning;
      /*--------resources--------*/
      //a vector to keep all the accesses all off regs, easier to clean up
      std::vector<RegAccess*> accessVector;
      
      //block map for accesses within blocks
      InBBAccsMap inBBAccs;
      //block map for accesses out side of blocks
      NotInBBAccsMap notInBBAccs;
      
      //block map for distance between the block begin to the next access
      BBDistMap distToBBBegin;//info to be used for merging
      //block map for distance between the block end to the next access
      BBDistMap distToBBEnd;  //info to be used to calculate distance to next access not in the current block

      /*----outside info needed for analysis----*/
      MachineFunction* MF;
      MachineRegisterInfo* MRI;
      //getting slotindex and within block distance
      //get all virtual regs/Live intervals and find accesses
      LiveIntervals *LIS;
      //dominator tree
      MachineDominatorTree* DT;
      MachinePostDominatorTree* PDT;
      MachineBranchProbabilityInfo* MBPI;

/*========member functions========*/
      //for spill coverage related
      MachineBasicBlock* GetLastDomBlock(MachineBasicBlock* mbb);
      //for getting dist from bb begin
      unsigned FirstDist(unsigned reg, MachineBasicBlock* mbb);
      //for checking use info update
      unsigned CheckCount(MachineBasicBlock* mbb);

      /*For multiple successors, merge distance to block end
                  Possible merge functions:
                  1. min
                  2. max
                  3. average
                  4-6. weigted 1-3
             */
      bool MergeDistancesChange(MachineBasicBlock* mbb);
      void AddRegAccess(unsigned reg);
      void PreAnalyzeAccess();
      void AnalyzeAccess();
      void AnalyzeDists();
      void Analyze();

      /*----this is for physical registers----*/
      void AnalyzePhysReg();

    public:
      /*---------for rpr use------------*/
      // for bb begin and such.
      RegAccess* GetFirstAccess(unsigned reg, MachineBasicBlock* mbb);
      RegAccess* GetLastAccess(unsigned reg, MachineBasicBlock* mbb);
      RegAccess* GetPrevAccess(unsigned reg, RegAccess* lastAcc);
      MachineInstr* GetFirstDef(unsigned reg, MachineBasicBlock* mbb);
      MachineInstr* GetPrevDef(unsigned reg, MachineInstr* def);
      MachineInstr* GetNextDef(unsigned reg, MachineInstr* def);
      MachineInstr* GetLastDef(unsigned reg, MachineBasicBlock* mbb);
      void InsertAccess(unsigned reg, MachineInstr* acc, bool isSpill);

      /*---------general public use*/
      static char ID; // Pass identification, replacement for typeid
      NextAccessAnalyzer();
      virtual ~NextAccessAnalyzer();
      void getAnalysisUsage(AnalysisUsage &AU) const override;
      void releaseMemory() override;
      /// runOnMachineFunction - pass entry point
      bool runOnMachineFunction(MachineFunction&) override;
      //TODO
      /// print - Implement the dump method.
      //void print(raw_ostream &O, const Module* = nullptr) const override;

      /*return the following
          0. infinity if dead after this instruction, i.e. can't find in dist to bb begin or end
          1. distance within the basic block if the next access is within the basic block
          2. if the spill for reg does not dominate all its accesses, the distance is the distance to the furthest mbb it dominates
          3. else return distance to the block end + distance of next access to the block end
       */
      unsigned GetDistance(unsigned reg, MachineInstr* mi, MachineBasicBlock* mbb);

      //turns out it would be really handy to have it available
      //get next access within block, null if none in block
      RegAccess* GetNextAccess(unsigned reg, MachineInstr* mi);

  };//NextAccessAnalyzer
}
#endif //NEXTACCESSANALYSIS_H



