#include <stdio.h>
void init_array(unsigned* array)
{
    unsigned i;
    for (i = 0; i < 100; i++){
        array[i] = i;
    }
}

int main()
{
    unsigned array[100];
    init_array(array);

    unsigned i;

    unsigned runtime = 100000;
    unsigned sum = 0;
    for (i = 0; i < runtime; i++){
      sum = 0;
      unsigned j;
      for (j = 0; j < 10000; j++){
          sum += array[j%100];
      }
    }

    printf("sum is %d\n", sum);
}
