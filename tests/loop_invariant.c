#include <stdio.h>
#include <stdlib.h>

int main(){
  //time_t t;
  //srand((unsigned) time(&t));
  unsigned a1 = rand()%4;
  unsigned a2 = rand()%4;
  unsigned a3 = rand()%4;
  unsigned a4 = rand()%4;
  unsigned invariant = rand()%8;


  unsigned i;
  unsigned bound = 1 << 29;
  for (i = 0; i < bound; i++){
    a1 += a2;
    a2 += a3;
    a3 += a4;
    a4 += a1;
    a1 = a1 >> 1;
    a2 = a2 >> 1;
    a3 = a3 >> 1;
    a4 = a4 >> 1;
  }

  unsigned sum = a1 + a2 + a3 + a4;
  if(sum < invariant)
    printf("sum %d is less than invariant %d\n", sum, invariant);
  else
    printf("sum %d is larger than invariant %d\n", sum, invariant);

}
